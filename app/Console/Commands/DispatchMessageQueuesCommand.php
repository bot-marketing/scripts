<?php

namespace App\Console\Commands;

use App\Modules\MessageQueues\Dispatcher;
use Illuminate\Console\Command;

class DispatchMessageQueuesCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'dispatch-message-queues';

    /**
     * Execute the command.
     *
     * @param \App\Modules\MessageQueues\Dispatcher $dispatcher
     */
    public function handle(Dispatcher $dispatcher)
    {
        $dispatcher->dispatch();
    }
}