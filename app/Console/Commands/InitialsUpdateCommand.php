<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Initial\InitialImportService;

class InitialsUpdateCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'initials:update 
                            {file : Файл scv} 
                            {--lang= : язык вгружаемых данных, пр. "ru"} 
                            {--type= : Тип: 1 - Имя, 2 - Фамилия}';

    /**
     * Execute the command.
     */
    public function handle()
    {
        $file = $this->argument('file');
        $language = $this->option('lang');
        $type = $this->option('type');

        (new InitialImportService($file, $language, $type))
            ->import();
    }
}