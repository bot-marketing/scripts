<?php

namespace App\Http\Controllers;

use App\Services\Adcome\ApiClient;
use Illuminate\Http\Request;

class AdcomeController extends Controller
{

    public function promocode(Request $request)
    {
        $this->validate($request, [
            'promo_standart' => 'required|string',
            'variable' => 'required|string',
            'method' => 'string',
        ]);

        try {
            $api = new ApiClient();
            $code = $api->get($request->query('method', $api::METHOD_PROMOCODE));
        }
        catch (\Exception $e) {
            $code = $request->get('promo_standart');
        }

        return [
            $this->updateVariables([
                $request->get('variable') => $code,
            ]),
        ];
    }
}