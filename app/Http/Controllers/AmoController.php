<?php

namespace App\Http\Controllers;

use App\Services\Amo\ApiClient;
use App\Services\Amo\ApiException;
use App\Services\Amo\Helper;
use GuzzleHttp\Exception\TransferException;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class AmoController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function findLead(Request $request)
    {
        return $this->wrap(function () use ($request) {
           $helper = $this->createAmoHelper($request);

           $lead = $this->resolveLeadId($request, $helper);

           return $this->saveLeadToField($request, $lead, [
               $this->systemMessage('Найден лид: '.$helper->urlToLead($lead)),
           ]);
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function addLead(Request $request)
    {
        return $this->wrap(function () use ($request) {
            $helper = $this->createAmoHelper($request);

            $contactPayload = $this->dataFromRequest($request, 'contact');
            $leadPayload = $this->dataFromRequest($request, 'lead');

            $contact = $helper->makeContact($request->json('payload.client'), $contactPayload);
            $lead = $helper->addLead($contact['id'], $leadPayload);

            return $this->saveLeadToField($request, $lead['id'], [
                $this->systemMessage("Создана сделка: ".$helper->urlToLead($lead['id'])),
            ]);
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $param
     *
     * @return mixed
     */
    protected function dataFromRequest(Request $request, $param)
    {
        try {
            return \GuzzleHttp\json_decode($request->query($param, '{}'), true);
        }

        catch (\Exception $e) {
            $response = response()->json([
                $this->systemMessage('Неверный параметр '.$param),
            ]);

            throw new HttpResponseException($response);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function updateLead(Request $request)
    {
        return $this->wrap(function () use ($request) {
            $helper = $this->createAmoHelper($request);

            if (!$data = $this->dataFromRequest($request, 'lead')) {
                return [ $this->systemMessage('Сделка не обновлена: нечего обновлять') ];
            }

            $helper->updateLead($leadId = $this->resolveLeadId($request, $helper), $data);

            return $this->saveLeadToField($request, $leadId, [
                $this->systemMessage("Сделка обновлена: ".$helper->urlToLead($leadId)),
            ]);
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function addNoteToLead(Request $request)
    {
        return $this->wrap(function () use ($request) {
            $helper = $this->createAmoHelper($request);

            if (!$text = $request->query('text')) {
                return [ $this->systemMessage('Комментарий не добавлен: поле text пусто') ];
            }

            $helper->addNoteToLead($leadId = $this->resolveLeadId($request, $helper), $text, $request->query('type', 4));

            return $this->saveLeadToField($request, $leadId, [
                $this->systemMessage('Добавлен комментарий к сделке: '.$helper->urlToLead($leadId)),
            ]);
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function updateContact(Request $request)
    {
        return $this->wrap(function () use ($request) {
            $helper = $this->createAmoHelper($request);

            if (!$data = $this->dataFromRequest($request, 'contact')) {
                return [ $this->systemMessage('Контакт не обновлен: нечего обновлять') ];
            }

            if (!$contact = $helper->findContact($request->json('payload.client'))) {
                return [ $this->systemMessage('Контакт не обновлен: не найден') ];
            }

            $helper->updateContact($contact['id'], $data);

            return [
                $this->systemMessage('Контакт обновлен'),
                $this->updateVariables([ 'amoContactId' => $contact['id'] ]),
            ];
        });
    }

    /**
     * @param $callback
     *
     * @return array
     */
    protected function wrap($callback)
    {
        try {
            return $callback();
        }

        catch (ApiException $e) {
            return [ $this->systemMessage('Ошибка AMO: '.$e->getMessage()) ];
        }

        catch (TransferException $e) {
            return [ $this->systemMessage('Ошибка передачи данных: '.$e->getMessage()) ];
        }

        catch (HttpResponseException $e) {
            throw $e;
        }

        catch (\Exception $e) {
            app(ExceptionHandler::class)->report($e);

            return [ $this->systemMessage('Ошибка: '.$e->getMessage()) ];
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @param $jobs
     *
     * @return array
     */
    protected function saveLeadToField(Request $request, $id, $jobs)
    {
        if ($field = $request->query('field')) {
            $jobs[] = $this->setClientField($field, $id);
        }

        $jobs[] = $this->updateVariables([ 'amoLeadId' => $id ]);

        return $jobs;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Amo\Helper $helper
     *
     * @return array|int|string|null
     *
     * @throws \Exception
     */
    protected function leadIdFromRequest(Request $request, Helper $helper)
    {
        if (($id = $request->query('id')) && (is_numeric($id))) {
            return $id;
        }

        return $helper->findLeadIdForClient($request->json('payload.client'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Services\Amo\Helper|array
     */
    protected function createAmoHelper(Request $request)
    {
        $domain = $request->query('domain');
        $login = $request->query('login');
        $hash = $request->query('hash');

        if (!$domain || !$login || !$hash) {
            return [ $this->systemMessage('Передайте параметры domain, login и hash') ];
        }

        return new Helper(new ApiClient($domain, $login, $hash));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed|null
     */
    public function leadsCallback(Request $request)
    {
        $subdomain = $request->get('account')['subdomain'];

        $api = new ApiClient("https://$subdomain.amocrm.ru", $request->query('amoLogin'), $request->query('amoHash'));

        // Get leads
        $leadIds = collect($request->get('leads'))->flatten(1)->pluck('id')->all();

        if (!$leads = $api->get('v2/leads', [ 'id' => $leadIds ])) return;

        // Get contacts
        $contactIds = collect($leads['_embedded']['items'])->pluck('main_contact.id')->unique()->all();

        if (!$contacts = $api->get('v2/contacts', [ 'id' => $contactIds ])) return;

        // Guess url
        if ($hookToken = $request->query('hookToken')) {
            $actions = rawurlencode($request->query('actions'));
            $url = "http://m.bot-marketing.com/api/hook/$hookToken/run?clientId=__clientId__&actions=$actions";
        } elseif ($triggerToken = $request->query('triggerToken')) {
            $query = Arr::except($request->query->all(), [ 'amoHash', 'amoToken', 'triggerToken' ]);
            $queryString = http_build_query($query);

            $url = "https://mssg.su/h/{$triggerToken}/__clientId__?{$queryString}";
        }

        // Run for all contacts
        foreach ($contacts['_embedded']['items'] as $contact) {
            if (preg_match('/\(client_id_(\d+)\)$/', $contact['name'], $matches)) {
                file_get_contents(str_replace('__clientId__', $matches[1], $url));
            }
        }

        echo microtime(true).PHP_EOL;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $helper
     *
     * @return array|int|string|null
     */
    protected function resolveLeadId(Request $request, $helper)
    {
        $leadId = $this->leadIdFromRequest($request, $helper);

        if (!$leadId) {
            throw new HttpResponseException(response()->json([ $this->systemMessage('Сделка не обновлена: не найдена') ]));
        }

        return $leadId;
    }
}