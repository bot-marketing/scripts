<?php


namespace App\Http\Controllers;

use App\Services\Bibicol\ApiClient;
use Illuminate\Http\Request;
use Zxing\QrReader;


class BibicolController extends Controller
{

    const QR_SERVER = 'http://api.qrserver.com/v1/';
    const QR_CONTENT_TYPE = 'multipart/form-data';

    const AMP_CHECK = 'https://api-dev.apmcheck.ru/api/';
    const AMP_CHECK_CONTENT_TYPE = 'application/json';
    // распознаем кртинку
    public function image(Request $request)
    {
        $this->validate($request, [
            'image' => 'required'
        ]);

        return $this->wrap(function () use ($request) {
            $apiKey = '';
            $url = self::QR_SERVER;
            $contentType = self::QR_CONTENT_TYPE;

            $image = $request->query('image');

            $data = [
                'fileurl' => $image
            ];

            $api = $this->createApiClient($apiKey, $url, $contentType);
            $result = $api->readQrCode($data);

            $bibicolParams = collect($result[0]['symbol'][0]);
            return [
                $this->updateVariables(['bibicolImage' => $bibicolParams])
            ];
        });
    }
    // добавление чека
    public function addCheck(Request $request)
    {
        $this->validate($request, [
            'fiscal' => 'required',
            'apikey' => 'required'
        ]);
        return $this->wrap(function () use ($request) {
            $apiKey = $request->query('apikey');
            $url = self::AMP_CHECK;
            $contentType = self::AMP_CHECK_CONTENT_TYPE;

            $fiscalString = urldecode($request->query('fiscal'));

            $data = [
                'qrStr' => $fiscalString
            ];

            $api = $this->createApiClient($apiKey, $url, $contentType);
            $result = $api->postQrReceipt($data);

            return [
                $this->updateVariables(['bibicolCheck' => $result])
            ];
        });
    }
    // проверка статуса чека
    public function retrieveResultOneReceipt(Request $request)
    {
        $this->validate($request, [
            'uuid' => 'required'
        ]);
        return $this->wrap(function () use ($request) {
            $apiKey = $request->query('apikey');
            $url = self::AMP_CHECK;
            $contentType = self::AMP_CHECK_CONTENT_TYPE;

            $uuid = $request->query('uuid');
            $api = $this->createApiClient($apiKey, $url, $contentType);

            $result = $api->receipt($uuid);

            return [
                $this->updateVariables(['bibicolReceipt' => $result])
            ];
        });
    }
//    ф-я для работы с библиотекой принимает путь до фото QR кода отдает то что в нем закодировано
//    тест проведен на  загрузке фото из проекта. Как реагирует на url не пробовал. НО должен.
//    public function QrRead()
//    {
//        return $this->wrap(function () {
//            $path = base_path() . '/public/img/test/';
//
//            $files = scandir($path);
//
//            $data = [];
//
//            foreach ($files as $file) {
//                if ($file == '.' || $file == '..') {
//                    continue;
//                } else {
//                    $qrdecode = new QrReader($path . $file);
//                    $data[$file] = $qrdecode->text();
//                }
//            }
//            app('log')->info($data);
//        });
//    }

    protected function wrap($callback)
    {
        try {
            return $callback();
        } catch (\Exception $e) {
            return [
                $this->systemMessage('Ошибка: ' . $e->getMessage())
            ];
        }
    }

    protected function createApiClient($apiKey, $url, $contentType)
    {
        return new ApiClient($apiKey, $url, $contentType);
    }

}