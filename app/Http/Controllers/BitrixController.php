<?php

namespace App\Http\Controllers;

use App\Services\Bitrix\ApiClient;
use App\Services\Bitrix\ApiException;
use App\Services\Bitrix\Helper;
use Carbon\Carbon;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class BitrixController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function addCommentToLead(Request $request)
    {
        return $this->wrap(function () use ($request) {
            $api = $this->createApiClient($request);
            $helper = new Helper($api);

            if (!$id = $this->leadIdFromRequest($request, $helper)) {
                return [ $this->systemMessage('Лид не найден') ];
            } elseif ($message = $request->json('query.message') ?: $request->query('message')) {
                $api->request('crm.livefeedmessage.add', [
                    'FIELDS' => [
                        'MESSAGE' => $message,
                        'ENTITYTYPEID' => 1,
                        'ENTITYID' => $id,
                    ],
                ]);

                return [ $this->systemMessage('Комментарий добавлен') ];
            } else {
                return [ $this->systemMessage('Комментарий не добавлен: Не указан текст') ];
            }
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Bitrix\Helper $helper
     *
     * @return array|int|string|null
     */
    protected function leadIdFromRequest(Request $request, Helper $helper)
    {
        if (!$id = $request->query('leadId')) {
            return $helper->findLead($request->json('payload.client'));
        } else {
            return $id;
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function leadUpdate(Request $request)
    {
        return $this->wrap(function () use ($request) {
            $api = $this->createApiClient($request);
            $helper = new Helper($api);

            if (!$id = $this->leadIdFromRequest($request, $helper)) {
                $result = [ $this->systemMessage('Лид не найден') ];
            } else {
                if (!($data = $request->query('lead')) || !is_array($data)) {
                    throw new \Exception('Неверные данные');
                }

                $id = $helper->updateLead($id, $data);

                $result = [ $this->systemMessage('Обновлен лид: '.$api->urlToItem('lead', $id)) ];
            }

            if (($field = $request->query('field')) && !$request->query('leadId')) {
                $result[] = $this->setClientField($field, $id);
            }

            $result[] = $this->updateVariables([ 'bitrixLeadId' => $id ]);

            return $result;
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lead(Request $request)
    {
        return $this->wrap(function () use ($request) {
            $api = $this->createApiClient($request);
            $helper = new Helper($api);

            if ($id = $helper->findLead($client = $request->json('payload.client'))) {
                $result = [ $this->systemMessage('Лид уже существует') ];
            } else {
                $id = $helper->addLead($client, $request->query('lead', []));

                $result = [ $this->systemMessage('Добавлен лид: '.$api->urlToItem('lead', $id)) ];
            }

            if ($field = $request->query('field')) {
                $result[] = $this->setClientField($field, $id);
            }

            $result[] = $this->updateVariables([ 'bitrixLeadId' => $id ]);

            return $result;
        });
    }

    /**
     * @param $callback
     *
     * @return array
     */
    protected function wrap($callback)
    {
        try {
            return $callback();
        }

        catch (\Exception $e) {
            app(ExceptionHandler::class)->report($e);

            return [ $this->systemMessage('Ошибка: '.$e->getMessage()) ];
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Services\Bitrix\ApiClient
     * @throws \Exception
     */
    protected function createApiClient(Request $request)
    {
        $domain = $request->query('domain');
        $id = $request->query('id');
        $token = $request->query('token');

        if (!$request->query('hook') && (!$domain || !$id || !$token)) {
            throw new \Exception('Не указаны необходимые параметры domain, id, token или hook');
        }

        return new ApiClient($this->hookUrl($request));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function deal(Request $request)
    {
        return $this->wrap(function () use ($request) {
            $apiClient = $this->createApiClient($request);

            $contactId = $this->findOrCreateContact($apiClient, $request);

            [ $dealId, $dealTitle ] = $this->addDealToExistingContact($apiClient, $request, $contactId);

            $actions = [
                $this->systemMessage("Добавлена новая сделка: ".$apiClient->urlToItem('deal', $dealId)),

                $this->updateVariables([
                    'bitrixContactId' => $contactId,
                    'bitrixDealId' => $dealId,
                ]),
            ];

            if ($field = $request->query('field')) {
                $actions[] = [
                    'type' => 'setClientField',
                    'payload' => [
                        'field' => $field,
                        'content' => $dealId,
                    ],
                ];
            }

            return $actions;
        });
    }

    /**
     * @param \App\Services\Bitrix\ApiClient $apiClient
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    protected function findOrCreateContact(ApiClient $apiClient, Request $request)
    {
        $phone = $request->json('payload.client.phone');

        if (!preg_match('/7\d{10}/', $phone)) $phone = null;

        $clientExternalId = $request->json('payload.client.externalId');

        $existingClients = $apiClient->request('crm.contact.list', [
            'filter' => [ 'ORIGINATOR_ID' => 'BotMarketing', 'ORIGIN_ID' => $clientExternalId ],
            'select' => [ 'ID' ],
        ]);

        if ($existingClients) return $existingClients[0]['ID'];

        if ($phone) {
            $existingClients = $apiClient->request('crm.contact.list', [
                'filter' => [ 'PHONE' => $phone ],
                'select' => [ 'ID' ],
            ]);

            if ($existingClients) {
                $apiClient->request('crm.contact.update', [
                    'id' => $existingClients[0]['ID'],

                    'fields' => [
                        'ORIGINATOR_ID' => 'BotMarketing',
                        'ORIGIN_ID' => $clientExternalId,
                    ],
                ]);

                return $existingClients[0]['ID'];
            }
        }

        $phoneData = [];

        if ($phone) {
            $phoneData['PHONE'] = [[
                'VALUE' => $phone,
                'VALUE_TYPE' => 'WORK',
                'TYPE_ID' => 'PHONE',
            ]];
        }

        return $apiClient->request('crm.contact.add', [
            'fields' => array_merge([
                'NAME' => $request->json('payload.client.name'),
                'TYPE_ID' => 'CLIENT',
                'OPENED' => 'Y',
                'ORIGINATOR_ID' => 'BotMarketing',
                'ORIGIN_ID' => $clientExternalId,
            ], $phoneData, $request->query('contact', [])),
        ]);
    }

    /**
     * @param \App\Services\Bitrix\ApiClient $apiClient
     * @param \Illuminate\Http\Request $request
     * @param $contactId
     *
     * @return array
     */
    protected function addDealToExistingContact(ApiClient $apiClient, Request $request, $contactId)
    {
        $title = $request->json('payload.client.name').' от '.Carbon::now('Europe/Moscow')->format('m.d.Y H:i');

        $id = $apiClient->request('crm.deal.add', [
            'fields' => array_merge([
                'TITLE' => $title,
                'OPENED' => 'Y',
                'CONTACT_ID' => $contactId,
                'ORIGINATOR_ID' => 'BotMarketing',
                'ORIGIN_ID' => $request->json('payload.client.externalId'),
            ], $request->query('deal', [])),

            'params' => [ 'REGISTER_SONET_EVENT' => 'Y' ],
        ]);

        return [ $id, $title ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    protected function hookUrl(Request $request)
    {
        if ($value = $request->query('hook')) {
            return rtrim($value, '/').'/';
        } else {
            $domain = rtrim($request->query('domain'), '/');
            $id = $request->query('id');
            $token = $request->query('token');

            return "https://$domain/rest/$id/$token/";
        }
    }
}