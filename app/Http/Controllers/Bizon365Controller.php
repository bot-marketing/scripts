<?php

namespace App\Http\Controllers;

use App\Services\Bizon365\ApiClient;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class Bizon365Controller extends Controller
{
    const UTM_ATTRS = [
        'campaign', 'term', 'source', 'medium', 'content', 'keyword', 'banner', 'phrase', 'group',
    ];

    public function getAllPages(Request $request)
    {
        $this->validate($request, [
            'pageId' => 'required',
        ]);

        return $this->wrap(function () use ($request) {
            $api = $this->createApiClient($request);
            $result = $api->subpages();
            $requestPageId = $request->query('pageId');
            $page = collect($result['pages'])->firstWhere('pageId', $requestPageId);

            $pageId = $page['pageId'];
            $roomId = $page['room'];
            $room = collect($result['rooms'][$roomId]);

            $closestDate = $room['closestDate'];
            $this->generateUserEmail();
            $bizonPage = compact('pageId', 'roomId', 'closestDate');

            return [
                $this->updateVariables([ 'bizonPage' => $bizonPage ]),
            ];
        });
    }

    public function registerUserOnWebinar(Request $request)
    {
        $this->validate($request, [
            'pageId' => 'required',
            'email' => 'required|email',
            'time' => 'required|date',
        ]);

        return $this->wrap(function () use ($request) {
            $data = [
                'pageId' => $request->query('pageId'),
                'email' => $request->query('email'),
                'time' => Carbon::parse($request->query('time'))->format('c'),
                'phone' => $request->query('phone'),
                'name' => $request->query('name'),
                'confirm' => 0,
            ];

            foreach (self::UTM_ATTRS as $key) {
                if ($value = $request->get("utm_$key")) {
                    $data["utm_$key"] = $value;
                }
            }

            $api = $this->createApiClient($request);

            $result = $api->addSubscriber($data);

            return [
                $this->updateVariables([ 'bizonRegistration' => $result ]),
            ];
        });
    }

    public function subscriber(Request $request)
    {
        $this->validate($request, [
            'pageId' => 'required',
            'hash' => 'required',
            'webinarTime' => 'required|date',
            'registeredTime' => 'required|date',
        ]);

        return $this->wrap(function () use ($request) {
            $api = $this->createApiClient($request);

//            $token = $request->query('token'); // ТОКЕН из запроса
            $pageId = $request->query('pageId'); // id комнаты
            $hash = $request->query('hash'); //  email клиента
            $WebinarTime = $request->query('webinarTime'); // начало вебинара // точное время

            /**/

            $registeredTime = $request->query('registeredTime'); // время регистрации пользователя
            $webinarTimeMin = $webinarTimeMax = Carbon::parse($WebinarTime)->format('c');
            $registeredTimeMin = Carbon::parse($registeredTime)->subMinutes(2)->format('c');
            $registeredTimeMax = Carbon::parse($registeredTime)->addMinutes(2)->format('c');

            $bizonSubscriber = compact('pageId', 'webinarTimeMin', 'webinarTimeMax', 'registeredTimeMin', 'registeredTimeMax');

            $result = $api->allSubscribers($bizonSubscriber);

            $list = collect($result['list']);

            $user = $list->first(function ($item) use ($hash) {
                return $item['secret'] == $hash;
            });

            if ($user) {
                return [
                    $this->updateVariables([ 'bizonSubscriber' => $user ]),
                ];
            }
        });
    }

    public function generateUserEmail()
    {
        $postfix = [ "com", "ru", "org", "su", "net", "biz" ];
        $beforeDog = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $afterDog = 'abcdefghijklmnopqrstuvwxyz';

        $randomStringBefore = '';
        $randomStringAfter = '';
        for ($i = 0; $i < 10; $i++) {
            $randomStringBefore .= $beforeDog[rand(0, strlen($beforeDog) - 1)];
        }
        for ($i = 0; $i < 5; $i++) {
            $randomStringAfter .= $afterDog[rand(0, strlen($afterDog) - 1)];
        }

        $rand_keys = array_rand($postfix, 1);
        $randPostfix = $postfix[$rand_keys];
        $randomUserEmail = $randomStringBefore.'@'.$randomStringAfter.'.'.$randPostfix;

        return $randomUserEmail;
    }

    protected function wrap($callback)
    {
        try {
            return $callback();
        }
        catch (\Exception $e) {
            return [ $this->systemMessage('Ошибка: '.$e->getMessage()) ];
        }
    }

    protected function createApiClient(Request $request)
    {
        if (!$token = $request->get('token')) {
            throw new \Exception('Не задан параметр token');
        }

        return new ApiClient($token);
    }

}