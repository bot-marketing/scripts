<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BookersonController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function makeBook(Request $request)
    {
        $client = new Client([ 'verify' => false, 'timeout' => 50 ]);

        try {
            $client->post('https://sample.bookerson.ru', [
                'query' => [
                    'makeBook' => $id = md5(time()),
                ],

                'form_params' => [
                    'sex' => $request->query('sex'),
                    'name' => $request->query('name'),
                    'mother' => $request->query('mother'),
                    'date' => Carbon::now()->subYears($request->query('age'))->format('d-m-Y'),
                ],
            ]);

            return [ $this->setClientField(-$request->query('field', -3), $id) ];
        }

        catch (\Exception $e) {
            return [ $this->systemMessage('Не удалось создать книгу') ];
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function discount(Request $request)
    {
        $baseAmount = $request->query('base_amount');
        $discount = $request->query('discount', '0%');
        $field = $request->query('field');
        $prepayment = $request->query('prepayment', '100%');

        if (!Str::endsWith($prepayment, '%')) {
            $result = $prepayment;
        } else {
            $prepayment = substr($prepayment, 0, -1);
            $discount = substr($discount, 0, -1);

            $result = $baseAmount * (100 - $discount) / 100 * ($prepayment / 100.0);
        }

        return [ $this->setClientField(-$field, $result) ];
    }
}