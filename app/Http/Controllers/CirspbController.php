<?php

namespace App\Http\Controllers;

use App\Services\Crispb\TemplateProcessor;
use Carbon\Carbon;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Http\Request;
use NcJoes\OfficeConverter\OfficeConverter;
use PhpOffice\PhpWord\Element\Image;

class CirspbController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     */
    public function __invoke(Request $request)
    {
        app('log')->debug('Cirspb', $request->all());

        $filename = 'cirspb-'.time();

        $data = $this->makeData($request);

        $docPath = $this->generateDoc($filename, $data);

//        $pdfPath = $this->convertToPdf($docPath, $filename);

        $url = $this->uploadToS3($filename, $docPath);

        unlink($docPath);
//        unlink($pdfPath);

        return response()->json([ 'url' => $url ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function callback(Request $request)
    {
        ($log = app('log'))->debug('CIRSPB', $request->all());

        if (!($id = $request->input('data.FIELDS_BEFORE.ID')) || !($triggerId = $request->get('triggerId'))) {
            $log->debug('CIRSPB invalid event');

            return;
        }

        $params = [
            'id' => $id,
            'select' => [ 'STATUS', 'UF_AUTO_224227502285', 'UF_CRM_TASK' ],
        ];

        $data = file_get_contents(env('CIRSPB_B24_HOOK').'/tasks.task.get?'.http_build_query($params));
        $data = json_decode($data, true)['result']['task'] ?? null;

        if ($data['status'] != 5) {
            $log->debug('CIRSPB Invalid task status', [ 'status' => $data['status'] ]);

            return;
        }

        if (!($chainData = $data['ufAuto224227502285'] ?? null)) {
            $log->debug('CIRSPB no chain data', compact('data'));

            return;
        }

        $chainData = json_decode(base64_decode($chainData), true);

        $log->debug('CIRSPB', compact('chainData'));

        if ($chainData['docN'] ?? false || !($clientId = $chainData['clientId'])) {
            $log->debug('CIRSPB docN filled or no client id specified');

            return;
        }

        if (!($dealId = mb_substr($data['ufCrmTask'][0] ?? '', 2))) {
            $log->debug('CIRSPB no deal id');

            return;
        }

        file_get_contents($url = 'https://mssg.su/h/'.$triggerId.'/'.$clientId.'?dealId='.$dealId.'&taskId='.$id);

        $log->debug('CIRSPB dispatched', compact('url'));
    }

    /**
     * @param \App\Services\Crispb\TemplateProcessor $template
     * @param array $images
     */
    protected function replaceImages(TemplateProcessor $template, array $images)
    {
        if (!$images) {
            $template->setValues([
                'images' => '',
                'image_1' => '',
                'image_2' => '',
                'image_3' => '',
                'image_4' => '',
                'image_5' => '',
                '${/images}' => '',
            ]);

            return;
        }

        $rowSize = 5;

        $rows = array_chunk($images, $rowSize);

        $template->cloneBlock('images', count($rows), true, true);

        foreach ($rows as $i => $row) {
            $rowN = $i + 1;

            if (count($row) < $rowSize) {
                $row = array_pad($row, $rowSize, '');
            }

            foreach ($row as $j => $v) {
                $colN = $j + 1;
                $key = "image_{$colN}#{$rowN}";

                if ($v) {
                    $template->setImageValue($key, [ 'path' => $v, 'width' => '5cm', 'height' => '5cm' ]);
                } else {
                    $template->setValue($key, $v);
                }
            }
        }
    }

    /**
     * @param \PhpOffice\PhpWord\TemplateProcessor $template
     * @param array $services
     */
    protected function replaceServices(TemplateProcessor $template, array $services): void
    {
        $template->cloneBlock('services', 0, true, false, collect($services)->map(function ($v) {
            return [ 'service' => $v ];
        })->all());
    }

    /**
     * @param \PhpOffice\PhpWord\TemplateProcessor $template
     * @param array $items
     */
    protected function replaceItems(TemplateProcessor $template, array $items): void
    {
        $rows = collect($items)->map(function ($v, $i) {
            if (!preg_match('/(?<name>.+?)( (?<type>[ABCD])?(?<qty>\d+))?$/i', $v, $matches)) return null;

            $letter = $matches['type'] ?? 'B';
            $item = "item{$letter}";

            return array_merge([
                'itemId' => ($i + 1).'.',
                'itemName' => $matches['name'],
                'itemA' => '',
                'itemB' => '',
                'itemC' => '',
                'itemD' => '',
            ], [
                $item => $matches['qty'] ?? 1,
            ]);
        })->filter();

        if ($rows->isEmpty()) {
            $template->cloneRowAndSetValues('itemId', [
                [
                    'itemId' => '',
                    'itemName' => '',
                    'itemA' => '',
                    'itemB' => '',
                    'itemC' => '',
                    'itemD' => '',
                ],
            ]);
        } else {
            $template->cloneRowAndSetValues('itemId', $rows->all());
        }
    }

    /**
     * @param \App\Services\Crispb\TemplateProcessor $template
     * @param array $dates
     */
    protected function replaceDates(TemplateProcessor $template, array $dates)
    {
        $re = '/(?<date>\d{1,2}\.\d{1,2}) (?<travelFrom>\d{1,2}) (?<from>\d{1,2}) (?<to>\d{1,2}) (?<travelTo>\d{1,2}) (?<people>\d{1,2})/i';

        $dates = collect($dates)->map(function ($v) use ($re) {
            if (!preg_match($re, $v, $matches)) return null;

            return [
                'date' => $matches['date'].'.'.date('Y'),
                'dateFrom' => $this->formatTime($matches['from']),
                'dateUntil' => $this->formatTime($matches['to'] ?? ''),
                'dateTravelFrom' => $this->formatTime($matches['travelFrom'] ?? ''),
                'dateTravelUntil' => $this->formatTime($matches['travelTo'] ?? ''),
                'dateEngineers' => $matches['people'] ?? 1,
            ];
        })->filter();

        $template->cloneRowAndSetValues('date', $dates->all());
    }

    /**
     * @param \App\Services\Crispb\TemplateProcessor $template
     * @param $stamp
     *
     * @throws \Exception
     */
    protected function replaceStamp(TemplateProcessor $template, $stamp)
    {
        $stamps = [
            1 => 'stamp_sir_spb.png',
            2 => 'stamp_navig.png',
            3 => 'stamp_sir_ptz.png',
        ];

        if (!isset($stamps[$stamp])) {
            throw new \Exception("Unknown stamp {$stamp}");
        }

        $template->replaceImage('stamp', storage_path("app/{$stamps[$stamp]}"));
    }

    /**
     * @param string $path
     * @param $filename
     *
     * @return string
     */
    protected function convertToPdf(string $path, $filename)
    {
        $converter = new OfficeConverter($path, $dir = storage_path('app'));
        $converter->convertTo($name = $filename.'.pdf');

        return $dir.'/'.$name;
    }

    /**
     * @param string $filename
     * @param string $filePath
     *
     * @return string
     */
    protected function uploadToS3(string $filename, string $filePath)
    {
        $disk = (new FilesystemManager(app()))->createS3Driver([
            'key' => env('AWS_ACCESS_KEY_ID'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'secret' => null,
            'visibility' => 'public',
        ]);

        $ext = pathinfo($filePath, PATHINFO_EXTENSION);

        $disk->writeStream($path = "cirspb/{$filename}.{$ext}", $stream = fopen($filePath, 'r'));

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $disk->url($path);
    }

    /**
     * @param string $filename
     * @param array $data
     *
     * @return string
     */
    protected function generateDoc(string $filename, array $data): string
    {
        $template = new TemplateProcessor(storage_path('app/cirspb.docx'));

        $template->setValues($data['params']);

//        $this->replaceServices($template, $data['services']);

        $this->replaceImages($template, $data['images']);

        $this->replaceItems($template, $data['items']);

        $this->replaceDates($template, $data['dates']);

        $this->replaceStamp($template, $data['stamp']);

        $template->saveAs($docPath = storage_path('app/'.$filename.'.docx'));

        return $docPath;
}

    /**
     * @return array
     */
    protected function testData(): array
    {
        $params = [
            'n' => '1234',
            'city' => 'Валуйки',
            'place' => 'ABCDE',
            'engineer' => 'Кальной',
            'customer' => 'Заказчик',
            'flag' => 'Россия',
            'imo_imsi' => '',
            'notes' => 'Оборудование предъявлено экипажу в исправном состоянии. Замечаний нет.',
        ];

        $services = [
            'Обновление туннелей',
            'Тест работы формата'
        ];

        $items = [ 'Пульт управления', 'Компасс земной 2 шт' ];

        $images = [
//            'https://storage.chat2desk.com/companies/company_60097/messages/35105883/client-1653605-21-8-47-903F6D78099328A50.jpg',
//            'https://storage.chat2desk.com/companies/company_60097/messages/35105730/client-1653605-21-8-45-02B850A5BD3B9DE62.jpg',
//            'https://storage.chat2desk.com/companies/company_60097/messages/35104181/client-1653605-21-8-25-8CB881ABC71F8682C.jpg',
//            'https://storage.chat2desk.com/companies/company_60097/messages/35103348/client-1653605-21-8-13-5187700877FE2D788.jpg',
//            'https://storage.chat2desk.com/companies/company_60097/messages/35003776/client-1653605-19-6-1-6D47B944512B2E651.jpg',
        ];

        $dates = [
            '21.09.2020 c 11:00 до 18:00 отправление в 7:00 прибытие в 22:00 3 чел',
            '22.09.2020 c 8:00 до 18:00 прибытие в 22:00',
            '23.09.2020 c 7:00 до 14:00 1 чел',
        ];

        $stamp = 2;

        return compact('params', 'services', 'images', 'items', 'dates', 'stamp');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function makeData(Request $request)
    {
        $deal = $request->get('deal');
        $answers = $request->get('answers');
        $ship = $request->get('ship');
        $user = $request->get('user');

        return [
            'params' => [
                'n' => $request->get('docN', 'НЕ ПОДТВЕРЖДЕН'),
                'city' => $deal['UF_CRM_1485273063'] ?? '',
                'place' => $ship['NAME'] ?? $ship['LAST_NAME'] ?? '',
                'engineer' => $this->mapEngineers($deal['UF_CRM_1503164223'] ?? ''),
                'customer' => '',
                'flag' => ($ship['UF_CRM_1552383324552'] ?? null) ?: $answers['flag'] ?? '',
                'imo_imsi' => $this->getIMOMMSI($ship),
                'notes' => $answers['jobs'] ?? '',
            ],
            'dates' => array_values($answers['dates']),
            'items' => array_values($answers['items'] ?? []),
            'images' => array_values($answers['photos'] ?? []),
            'stamp' => $answers['stamp'],
        ];
    }

    /**
     * @param $ship
     *
     * @return mixed|string
     */
    protected function getIMOMMSI($ship)
    {
        $result = $ship['UF_CRM_1508500822'] ?? '';

        if ($value = $ship['UF_CRM_1545371205405'] ?? '') {
            $result .= ' / '.$value;
        }

        return $result;
    }

    /**
     * @param $time
     *
     * @return string
     */
    protected function formatTime($time)
    {
        return $time > 0 ? Carbon::createFromTime($time, 0, 0, 'Europe/Moscow')->format('H:i') : '';
    }

    /**
     * @param $value
     *
     * @return string
     */
    protected function mapEngineers($value)
    {
        return collect(explode(',', $value))->map(function ($v) {
            if (preg_match('/(!.+!)?(?<firstName>.+) (?<lastName>.+) \[/u', trim($v), $matches)) {
                return $matches['lastName'].' '.mb_substr($matches['firstName'], 0, 1);
            } else {
                return null;
            }
        })->filter()->implode(', ');
    }
}