<?php

namespace App\Http\Controllers;

use App\Modules\Initials\Initial;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\Debug\ExceptionHandler;

class CommonController extends Controller
{
    /**
     * @var \Illuminate\Cache\Repository
     */
    protected $cache;

    /**
     * CommonController constructor.
     */
    public function __construct()
    {
        $this->cache = app('cache')->store();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function startTunnelBySwitch(Request $request)
    {
        $case = $request->query('case');
        $value = $request->query('value');

        if (is_array($case) && $value && isset($case[$value])) {
            return [
                ['type' => 'tunnelStart', 'payload' => $case[$value]],
            ];
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function sleep(Request $request)
    {
        $this->validate($request, ['time' => 'required|numeric|min:0|max:10']);

        usleep($request->get('time') * 1000000);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getInitial(Request $request)
    {
        $this->validate($request, [
            'text' => 'required',
            'need' => 'nullable',
        ]);

        $text = Initial::parseText($request->get('text'));

        try {
            $variables = [];
            $need = $request->get('need');

            if ((!$need || $need == 'firstName') && ($value = Initial::relevationRaw($text, Initial::TYPE_NAME))) {
                $variables['firstName'] = $value->value;
                $variables['sex'] = Initial::sexToText($value->sex);
                $text = Initial::getLeftRightWords($text, mb_strtolower($variables['firstName']));
            }

            if ((!$need || $need == 'lastName') && ($value = Initial::relevationRaw($text, Initial::TYPE_SURNAME))) {
                $variables['lastName'] = $value->value;
                $variables['sex'] = $variables['sex'] ?? Initial::sexToText($value->sex);
            }

            if ($variables) {
                return [$this->updateVariables($variables)];
            }

            return [];
        } catch (\Exception $e) {
            app(ExceptionHandler::class)->report($e);

            return [$this->systemMessage('Ошибка: ' . $e->getMessage())];
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function storeValue(Request $request)
    {
        $value = $request->request->get('value');
        $key = $request->request->get('key') ?: Str::random(8);

        $this->cache->put($key, $value, Carbon::now()->addMinutes(5));

        return response($key, 200, [
            'Access-Control-Allow-Origin' => '*',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function restoreValue(Request $request)
    {
        if ($value = $this->cache->get($request->query('key'))) {
            return [
                $this->updateVariables([
                    'storedValue' => $value,
                ]),
            ];
        }
    }
}