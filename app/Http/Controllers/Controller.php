<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param array $errors
     *
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        $message = 'Ошибка данных:'.PHP_EOL.implode(PHP_EOL, Arr::flatten($errors));

        if ($request->input('payload.client') !== null) {
            return new JsonResponse([ $this->systemMessage($message) ]);
        } else {
            return response($message, 400);
        }
    }

    /**
     * @param $text
     *
     * @return array
     */
    public function systemMessage($text)
    {
        return [ 'type' => 'sendSystemMessage', 'payload' => $text ];
    }

    /**
     * @param $fieldId
     * @param $value
     *
     * @return array
     */
    public function setClientField($fieldId, $value)
    {
        return [
            'type' => 'setClientField',
            'payload' => [
                'field' => $fieldId,
                'content' => $value,
            ],
        ];
    }

    /**
     * @param $variables
     *
     * @return array
     */
    public function updateVariables($variables)
    {
        return [
            'type' => 'updateVariables',
            'payload' => compact('variables'),
        ];
    }

    /**
     * @param $triggerId
     * @param $clientId
     * @param null $params
     */
    public function callTrigger($triggerId, $clientId, $params = null)
    {
        $url = "https://mssg.su/h/$triggerId/$clientId?".http_build_query($params);

        file_get_contents($url);
    }
}
