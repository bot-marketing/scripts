<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Utils;
use Illuminate\Http\Request;

class FacedetectorController extends Controller
{
    public function __invoke(Request $request)
    {
        $data = [];

        foreach ($request->all() as $k => $v) {
            if ($k !== 'face') {
                $data[] = [ 'name' => $k, 'contents' => $v ];

                continue;
            }

            $data[] = [
                'name' => $k,
                'contents' => Utils::tryFopen($v, 'r'),
                'filename' => pathinfo($v, PATHINFO_FILENAME).'.'.pathinfo($v, PATHINFO_EXTENSION),
            ];
        }

        $response = (new Client([ 'http_errors' => false, 'timeout' => 30 ]))
            ->post('https://facedetector.ru/external/tg/upload-and-search', [
                'multipart' => $data,
            ]);

        return response($response->getBody()->getContents(), $response->getStatusCode(), $response->getHeaders());
    }
}