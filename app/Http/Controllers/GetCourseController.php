<?php

namespace App\Http\Controllers;

use App\Services\GetCourse\ApiClient;
use App\Services\GetCourse\ApiException;
use Illuminate\Http\Request;

class GetCourseController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function addUser(Request $request)
    {
        $domain = $request->query('domain');
        $key = $request->query('key');
        $email = $request->query('email');

        if (!$domain || !$key || !$email) return [ $this->systemMessage('Не указаны параметры domain, key, email') ];

        try {
            $userData = \GuzzleHttp\json_decode($request->query('user', '[]'), true);
            $sessionData = \GuzzleHttp\json_decode($request->query('session', '[]'), true);
        }

        catch (\Exception $e) {
            return [ $this->systemMessage('Плохой параметр data') ];
        }

        $api = new ApiClient($domain, $key);

        try {
            $data = [ 'email' => $email ];

            if ($field = $request->query('field')) {
                $data['addfields'][$field] = $request->json('payload.client.externalId');
            }

            $result = $api->post('users', 'add', $data = [
                'user' => array_filter($userData + $data),
                'system' => [ 'refresh_if_exists' => 1 ],
                'session' => $sessionData,
            ]);

            app('log')->debug('[GETCOURSE] sent data', $data);
        }

        catch (\Exception $e) {
            return [ $this->systemMessage('Ошибка: '.$e->getMessage()) ];
        }

        if ($result['error'] ?? false) {
            return [ $this->systemMessage('Ошибка: '.($result['error_message'] ?? 'неизвестная ошибка')) ];
        }

        $action = $result['user_status'] === 'added' ? 'добавлен' : 'обновлен';
        $url = "https://$domain/user/control/user/update/id/{$result['user_id']}";

        return [
            $this->systemMessage("Пользователь $action: $url"),

            $this->updateVariables([
                'getcourseData' => $result,
            ])
        ];
    }
}