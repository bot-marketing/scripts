<?php

namespace App\Http\Controllers;

use App\Services\Google\BigQueryHelper;
use Google\Service\Bigquery\TableDataInsertAllRequest;
use Google\Service\Exception as GoogleServiceException;
use Illuminate\Http\Request;

class GoogleBigQueryController extends Controller
{
    public function insertAll(Request $request)
    {
        $this->validate($request, [
            'projectId' => 'required',
            'datasetId' => 'required',
            'tableId' => 'required',
            'body' => 'required',
        ]);

        return $this->wrap($request, function (BigQueryHelper $helper, Request $request) {
            $input = $request->input();

            $response = $helper->insertAll(
                $input['datasetId'],
                $input['tableId'],
                new TableDataInsertAllRequest($input['body']),
            );

            if ($errors = $response->getInsertErrors()) return $errors;
        });
    }

    protected function wrap(Request $request, $callback)
    {
        try {
            $input = $request->input();

            return $callback(
                new BigQueryHelper(
                    $input['projectId'],
                    $request->header('X-BotMarketing-User'),
                    $input['serviceAuthFile']
                ), $request);
        }

        catch (GoogleServiceException $e) {
            return response($e->getMessage(), $e->getCode(), [
                'content-type' => 'application/json',
            ]);
        }
    }
}