<?php

namespace App\Http\Controllers;

use App\Services\Google\SheetsHelper;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;

class GoogleSheetsController extends Controller
{
    public function addRow(Request $request)
    {
        $this->validate($request, [
            'fileId' => 'required',
            'sheetId' => 'nullable|numeric|min:0',
            'row' => 'required',
            'serviceAuthFile' => 'nullable|string|regex:/^[a-z0-9-_]+$/i',
        ]);

        $webhook = $request->input('payload.client') !== null;

        if (is_string($row = $request->get('row'))) {
            try {
                $row = json_decode($row, true, JSON_THROW_ON_ERROR);
            }

            catch (\JsonException $e) {
                return response()->json([ 'error' => $e->getMessage() ], 400);
            }
        }

        if (!$row || !is_array($row)) {
            if ($webhook) {
                return [ $this->systemMessage('Данные не заполнены') ];
            } else {
                return response()->json([ 'error' => 'Данные не заполнены' ], 400);
            }
        }

        return $this->wrap($request, function (SheetsHelper $service) use ($request, $row, $webhook) {
            if (!($result = $service->addRow($row, $request->get('sheetId', 0))) && !$webhook) {
                return response()->json([ 'error' => 'Не удалось добавить строку' ], 403);
            }

            return $webhook ? [ $this->systemMessage($result ? 'Строка добавлена' : 'Строка НЕ добавлена') ] : null;
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getRows(Request $request)
    {
        $this->validate($request, [
            'fileId' => 'required',
            'range' => 'required',
            'asObject' => 'nullable|boolean',
            'keyBy' => 'nullable|string',
        ]);

        return $this->wrap($request, function (SheetsHelper $h) use ($request) {
            $var = $request->query('var', 'rows');

            return [
                $this->updateVariables([
                    $var => $h
                        ->getRows($request->query('range'), $request->get('asObject'))
                        ->when($request->get('where'), function ($items, $where) {
                            $where = json_decode($where, true);

                            if (!$where || !isset($where[0]) || !isset($where[1])) {
                                return $items;
                            }

                            if (count($where) > 2 && $where[1] === 'like') {
                                $pattern = '/^'.strtr(
                                        preg_quote($where[2], '/'), [ '%' => '.*', '\\\b' => '\b', '\?' => '.' ]
                                    ).'$/ui';

                                return $items->filter(function ($item) use ($where, $pattern) {
                                    return preg_match($pattern, $item[$where[0]] ?? null);
                                });
                            } else {
                                return $items->where(...$where);
                            }
                        })
                        ->values()
                        ->when($request->query('keyBy') !== null, function ($items) use ($request) {
                            return $items->keyBy($request->query('keyBy'));
                        })
                ])
            ];
        });
    }

    public function updateRow(Request $request)
    {
        $this->validate($request, [
            'fileId' => 'required|string',
            'range' => 'required|string',
            'row' => 'required|array',
            'serviceAuthFile' => 'nullable|string|regex:/^[a-z0-9-_]+$/i',
        ]);

        return $this->wrap($request, function (SheetsHelper $h) use ($request) {
            return $h->updateRow($request->get('range'), $request->get('row'));
        });
    }

    public function batchUpdate(Request $request)
    {
        $this->validate($request, [
            'fileId' => 'required|string',
            'requests' => 'required|array',
            'serviceAuthFile' => 'nullable|string|regex:/^[a-z0-9-_]+$/i',
        ]);

        return $this->wrap($request, function (SheetsHelper $h) use ($request) {
            return $h->batchUpdate($request->get('requests'));
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $callback
     *
     * @return mixed
     */
    protected function wrap(Request $request, $callback)
    {
        $fileId = $request->get('fileId');
        $webhook = $request->input('payload.client') !== null;
        $quotaUser = $request->header('X-BotMarketing-User');

        if (!$serviceAuthFile = $request->input('serviceAuthFile')) {
            $limiter = new RateLimiter(app('cache')->store());

            if ($limiter->tooManyAttempts($key = "gtrw:".($quotaUser ?: $fileId), $quotaUser ? 60 : 10)) {
                return response('', 429);
            }

            $limiter->hit($key);
        }

        try {
            return $callback(new SheetsHelper($fileId, $serviceAuthFile, $quotaUser, !!$request->get('parseDates', false), $request->get('tz')));
        }

        catch (ConnectException $e) {
            if (!$webhook) return response('connect_error', 400);

            return [ $this->systemMessage('Ошибка: connect_error') ];
        }

        catch (\Google\Service\Exception $e) {
            if (!$webhook) return response($e->getMessage(), $e->getCode());

            return [ $this->systemMessage('Ошибка: '.$e->getMessage()) ];
        }

        catch (\Google_Exception $e) {
            if (!$webhook) return response($e->getMessage(), 400);

            return [ $this->systemMessage('Ошибка: '.$e->getMessage()) ];
        }
    }
}