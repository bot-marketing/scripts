<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class KonturController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function start(Request $request)
    {
        $date = $request->query('date');
        $short = $request->query('short');
        $long = $request->query('long');

        if (!$date || !$short || !$long) {
            return $this->error('Неверный запрос');
        }

        $date = Carbon::parse($date, 'Europe/Moscow');

        if ($date->isPast()) return null;

        $date->subDays($request->query('days', 14));

        $tunnel = $date->isFuture() ? $long : $short;

        return [
            [ 'type' => 'tunnelStart', 'payload' => $tunnel ],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function bonus(Request $request)
    {
        if (!$client = $request->json('payload.client.externalId')) {
            return $this->error('Неверный запрос');
        }

        /**
         * @var \Illuminate\Cache\Repository $cache
         */
        $cache = app('cache');

        $key = "kontur.bonus.$client";

        if ($request->has('reset')) {
            $bonus = 1;
        } else {
            $bonus = min(5, $cache->get($key, 0) + 1);
        }

        $cache->put($key, $bonus, Carbon::now()->addDays(10));

        return [
            [ 'type' => 'sendImage', 'payload' => "http://scripts.bot-marketing.com/img/$bonus-star.png" ],
        ];
    }

    /**
     * @param $text
     *
     * @return array
     */
    protected function error($text): array
    {
        return [
            [ 'type' => 'sendSystemMessage', 'payload' => $text ],
        ];
}
}