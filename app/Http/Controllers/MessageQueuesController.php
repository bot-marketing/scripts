<?php

namespace App\Http\Controllers;

use App\Modules\MessageQueues\MessageQueue;
use App\Services\BotCorp\ApiClient;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MessageQueuesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function push(Request $request)
    {
        $this->validate($request, [
            'api_token' => 'required',
            'phone' => 'required|regex:/^7\d{10}$/',
            'name' => 'required|string|max:255',
            'text' => 'required_without:file_url|string|max:5000|min:10',
            'file_url' => 'required_without:text|url',
            'time' => 'required|date_format:H:i',
            'timezone' => 'nullable|timezone',
        ]);

        $time = Carbon::now($request->query('timezone', '+03:00'))->setTimeFromTimeString($request->query('time').':00');

        if ($time->isPast()) {
            return [ $this->systemMessage('Время отправки уже прошло.') ];
        }

//        $this->validatePhone($request);

        $queue = MessageQueue::findOrCreate(
            MessageQueue::P_BOTCORPIO,
            $request->query('api_token'),
            $request->query('name'),
            $text = $request->query('text'),
            $url = $request->query('file_url'),
            $time
        );

        if (!$queue->pushContact($request->query('phone'))) {
            return [ $this->systemMessage('Контакт уже добавлен.') ];
        }

        return $this->formatResult($time, $url, $text);
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    protected function validatePhone(Request $request)
    {
        try {
            $api = new ApiClient($request->query('api_token'));

            $phoneInfo = $api->get('whatsapp_phone_get', [ 'phone' => $request->query('phone') ]);

            if ($phoneInfo['status'] != 1) {
                throw new HttpResponseException(new JsonResponse([
                    $this->systemMessage('Данный контакт не подписан'),
                ]));
            }
        }

        catch (\Exception $e) {
            throw new HttpResponseException(new JsonResponse([
                $this->systemMessage('botcorp.io error: '.$e->getMessage()),
            ]));
        }
    }

    /**
     * @param \Carbon\Carbon $time
     * @param $url
     * @param $text
     *
     * @return array
     */
    protected function formatResult(Carbon $time, $url, $text): array
    {
        $result = 'Добавлено в очередь, время отправки: '.$time->format('d.m.Y H:i:s');

        if ($url) {
            $result .= PHP_EOL.'Файл: '.$url;
        }

        if ($text) {
            $result .= PHP_EOL.'Сообщение:'.PHP_EOL.PHP_EOL.$text;
        }

        return [ $this->systemMessage($result) ];
    }
}