<?php

namespace App\Http\Controllers;


use App\Services\Motorland\ApiClient;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class MotorlandController extends Controller
{
    /**
     * Получение данных клиента
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function clientInfo(Request $request)
    {
        $this->validate($request, [
            'sn'                   => 'required|string',
            'snid'                 => 'required|string',
            'payload.client.phone' => 'required',
            'production'           => 'required|integer',
            'api_url'               => 'nullable|string',
        ]);

        try {
            $api = new ApiClient($request->query('production'), $request->query('api_url'));

            $data = $api->info([
                'sn'    => $request->query('sn'),
                'snid'  => $request->query('snid'),
                'phone' => $request->json('payload.client.phone'),
            ]);


            $response = [
                $this->updateVariables([
                    'clientData' => $data->toArray()
                ])
            ];

            if ($data->get('Chat2DeskId')) {
                $response [] = [
                    'type'    => 'transferToOperator',
                    'payload' => [
                        'id' => $data->get('Chat2DeskId'),
                    ],
                ];
            }

            return $response;

        } catch (\Exception $e) {
            if (strripos($e->getMessage(), 'client not found') === false) {
                return [$this->systemMessage('Ошибка: ' . $e->getMessage())];
            }
        }
    }


    /**
     * @param Request $request
     *
     * @return array
     */
    public function clientSave(Request $request)
    {
        $this->validate($request, [
            'sn'                        => 'required|string',
            'snid'                      => 'required|string',
            'clientId'                  => 'required|integer',
//            'Chat2DeskId'               => 'required',
            'payload.client.phone'      => 'required',
            'payload.client.name'       => 'nullable|string',
            'payload.dialog.externalId' => 'required|integer',
            'production'                => 'required|integer',
            'api_url'                    => 'nullable|string',
        ]);

        try {
            $api = new ApiClient($request->query('production'), $request->query('api_url'));

            $api->save([
                'sn'          => $request->query('sn'),
                'snid'        => $request->query('snid'),
                'clientId'    => $request->query('clientId'),
//                'Chat2DeskId' => $request->query('Chat2DeskId'),
                'phone'       => $request->json('payload.client.phone'),
                'clientName'  => $request->json('payload.client.name'),
                'url'         => 'https://web.chat2desk.com/chat/all?dialogID=' . $request->json('payload.dialog.externalId'),
            ]);

        } catch (\Exception $e) {
            return [$this->systemMessage('Ошибка: ' . $e->getMessage())];
        }
    }


}