<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class OpArtController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
          'table' => 'filled',
          'date_created_start' => 'required|date',
          'date_created_end' => 'required|date',
        ]);

        $table = $request->input('table', 'clients_survey_1');

        $rows = app('db')->connection('pgsql')->table($table)
          ->whereBetween('date_created', [
            $request->input('date_created_start', Carbon::now()->startOfDay()),
            $request->input('date_created_end', Carbon::now()->endOfDay())
          ])
          ->get();

        return response()->json($rows);
    }
}