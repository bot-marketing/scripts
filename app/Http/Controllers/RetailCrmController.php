<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use RetailCrm\ApiClient;
use RetailCrm\Exception\CurlException;

class RetailCrmController extends Controller
{

    public function createOrder(Request $request)
    {
        $this->validate($request, [
            'domain' => 'required|url',
            'token' => 'required',
            'site' => 'required',
            'orderData' => 'nullable',
            'customerData' => 'nullable',
        ]);

        if (!($domain = $request->query('domain')) || !($token = $request->query('token'))) {
            return [ $this->systemMessage('Необходимо указать domain и token') ];
        }

        $site = $request->query('site');
        $payload = $request->json('payload');
        $fullName = explode(' ', $payload['client']['name']);

        $firstName = $payload['data']['firstName'] ?? $fullName[0];
        $lastName = $payload['data']['lastName'] ?? $fullName[1] ?? '';
        $middleName = $payload['data']['middleName'] ?? null;
        $phone = $payload['contactPhone'] ?: $payload['client']['phone'] ?: null;
        $email = $payload['data']['email'] ?? null;

        $apiClient = new ApiClient($domain, $token, ApiClient::V5);

        $extId = $payload['client']['externalId'];

        try {
            $response = $apiClient->request->customersGet($extId, 'externalId', $site);    // Looking for client
        }

        catch (CurlException $e) {
            return [ $this->systemMessage('Connection error: '.$e->getMessage()) ];
        }

        if ($response->getStatusCode() == 404) {       // Client not found
            try {
                $data = [
                    'externalId' => $extId,
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'patronymic' => $middleName,
                    'phone' => $phone,
                    'email' => $email,
                ];

                $data = array_merge($data, Arr::wrap(json_decode($request->query('customerData'), true)));

                $apiClient->request->customersCreate($data, $site);
            }

            catch (CurlException $e) {
                return [ $this->systemMessage('Connection error: '.$e->getMessage()) ];
            }
        }

        try {
            $orderData = array_merge_recursive([
                'externalId' => $payload['id'],
                'site' => $site,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'patronymic' => $middleName,
                'phone' => $phone,
                'email' => $email,
                'items' => $this->mapItems($payload),
                'customerComment' => $payload['clientComment'],
                'managerComment' => $payload['managerComment'],
                'customer' => [
                    'externalId' => $extId,
                ],
                'delivery' => $this->makeDelivery($payload),
            ],
                json_decode($request->query('orderData'), true) ?? []
            );

            app('log')->debug('[RETAILCRM] new order', compact('orderData', 'payload'));

            $response = $apiClient->request->ordersCreate($orderData, $site);
        }
        catch (CurlException $e) {
            return [ $this->systemMessage('Connection error: '.$e->getMessage()) ];
        }

        if ($response->isSuccessful() && 201 === $response->getStatusCode()) {
            $orderId = $response->id;
            $url = $domain.'/orders/'.$orderId.'/edit';

            return [ $this->updateVariables([ 'retailCRM' => compact('orderId', 'url') ]) ];
        } else {
            $error = sprintf("Error: [HTTP-code %s] %s", $response->getStatusCode(), $response->getResponseBody());
            return [ $this->systemMessage($error) ];
        }
    }

    public function editOrder(Request $request)
    {
        $this->validate($request, [
            'domain' => 'required|url',
            'token' => 'required',
            'site' => 'required',
            'orderData' => 'required',
        ]);

        $payload = $request->json('payload');
        $domain = $request->query('domain');
        $token = $request->query('token');

        $apiClient = new ApiClient($domain, $token, ApiClient::V5);

        try {
            $data = json_decode($request->query('orderData'), true) ?? [];

            if ($value = $request->query('orderExternalId')) {
                $data[$by = 'id'] = $value;
            } else {
                $data[$by = 'externalId'] = $payload['id'];
            }

            app('log')->debug('[RETAILCRM] update order', compact('data'));

            $response = $apiClient->request->ordersEdit($data, $by, $request->query('site'));
        }
        catch (CurlException $e) {
            return [ $this->systemMessage('Connection error: '.$e->getMessage()) ];
        }

        if ($response->isSuccessful() && 200 === $response->getStatusCode()) {
            $orderId = $response->id;
            $url = $domain.'/orders/'.$orderId.'/edit';
            return [ $this->updateVariables([ 'retailCRM' => compact('orderId', 'url') ]) ];
        } else {
            $error = sprintf("Error: [HTTP-code %s] %s", $response->getStatusCode(), $response->getResponseBody());
            return [ $this->systemMessage($error) ];
        }
    }

    /**
     * @param $payload
     *
     * @return array
     */
    public function mapItems($payload): array
    {
        return collect($payload['items'])->map(function ($item) {
            return array_filter([
                'productName' => $item['name'],
                'initialPrice' => $item['originalPrice'],
                'discountManualAmount' => $item['originalPrice'] - $item['price'],
                'quantity' => $item['quantity'],
                'externalId' => $item['id'],
                'url' => $item['url'],
                'imageUrl' => $item['tinyImage'],
                'offer' => $item['code'] ? [ 'externalId' => $item['code'] ] : null,
            ]);
        })->all();
    }

    /**
     * @param $payload
     *
     * @return array
     */
    protected function makeDelivery($payload)
    {
        $result = [
            'cost' => $payload['deliveryCost'],
        ];

        $dt = $payload['deliveryType'];

        if (isset($payload['city'])) {
            $result['address']['city'] = $payload['city']['name'];
        }

        if (($payload['data']['deliveryService'] ?? null) === 'sdek') {
            $result['data']['tariffType'] = $payload['data']['deliveryServiceData']['tariffId'] ?? null;

            if (isset($payload['city'])) {
                $result['data']['receiverCity'] = $payload['city']['sdek'];
            }

            if (isset($payload['pickupPoint'])) {
                $result['data']['pickuppointId'] = $payload['pickupPoint']['deliveryServiceId'];
                $result['address']['text'] = $payload['pickupPoint']['name'];
            }
        } elseif ($dt['type'] === 'pickup') {
            $result['data']['pickuppointId'] = $payload['pickupPoint']['name'];
        }

        if ($dt['type'] === 'delivery') {
            $result['address']['street'] = $payload['address']['street'];
            $result['address']['flat'] = $payload['address']['apartment'];
            $result['address']['building'] = $payload['address']['streetNumber'];
            $result['address']['house'] = $payload['address']['building'];
        }

        return $result;
    }

}