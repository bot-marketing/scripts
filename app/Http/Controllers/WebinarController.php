<?php

namespace App\Http\Controllers;

use App\Services\Webinar\ApiClient;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class WebinarController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param $eventId
     *
     * @return array
     */
    public function latestEventSession(Request $request, $eventId)
    {
        return $this->wrap(function () use ($request, $eventId) {
            $api = $this->createApiClient($request);

            $data = $api->get('organization/events/'.$eventId);

            $webinarSession = collect($data['eventSessions'])
                ->sortBy('utcStartsAt')
                ->first(function ($item) {
                    return $item['utcStartsAt'] > time();
                });

            $webinarSession = Arr::only($webinarSession, [
                'id', 'name', 'description', 'startsAt',
            ]);

            return [ $this->updateVariables(compact('webinarSession')) ];
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $eventId
     *
     * @return array
     */
    public function registerToEventSession(Request $request, $eventId)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        return $this->wrap(function () use ($request, $eventId) {
            $api = $this->createApiClient($request);

            $data = [
                'email' => $request->query('email'),
                'nickname' => $request->json('payload.client.name'),
                'phone' => $request->json('payload.client.phone'),
                'sendEmail' => 0,
                'additionalFields' => $request->query('fields'),
            ];

            $result = $api->post("eventsessions/{$eventId}/register", array_filter($data));

            return [ $this->updateVariables([ 'webinarRegistration' => $result ]) ];
        });
    }

    /**
     * @param $callback
     *
     * @return array
     */
    protected function wrap($callback)
    {
        try {
            return $callback();
        }

        catch (\Exception $e) {
            return [ $this->systemMessage('Ошибка: '.$e->getMessage()) ];
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Services\Webinar\ApiClient
     * @throws \Exception
     */
    protected function createApiClient(Request $request)
    {
        if (!$token = $request->get('token')) {
            throw new \Exception('Не задан параметр token');
        }

        return new ApiClient($token);
    }
}