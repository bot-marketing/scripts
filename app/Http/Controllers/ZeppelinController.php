<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ZeppelinController extends Controller
{
    const SITES = [
        'CAT' => [
            'key' => 'YFQR96o6x8sSk8InF59Td6eEYxUdhNLe',
            'url' => 'https://www.zeppelin.ru/',
            'phone' => '8 800 707-91-65',
            'phone_by_source' => [
                'avito' => '8 800 707-91-65',
                'drom' => '8 800 707-81-93',
                'exkavator' => '8 800 350-65-03',
                'gruzovik' => '8 800 350-13-65',
            ],
            'email' => [
                'login' => 'zrs-msk-rek',
                'password' => 'lsetl8w38fnjsld',
            ],
        ],

        'SEM' => [
            'key' => '1o3xYNe_x_bxpYzygHL8l8f80VUgQZiQ',
            'url' => 'https://sem-rus.ru/',
            'phone' => '8 800 350-52-37',
            'phone_by_source' => [
                'avito' => '8 800 350-52-37',
                'drom' => '8 800 707-86-48',
                'exkavator' => '8 800 100-84-61',
                'gruzovik' => '8 800 350-13-49',
            ],
            'email' => [
                'login' => 'zrs-msk-semrek',
                'password' => 'lsetl7w37fnjsld',
            ],
        ],

        'HF' => [
            'key' => 'u8LmztkKgGa77Zz6cIxDu3Oq5E2pqySV',
            'url' => 'https://heavyfair.com/',
            'phone' => '8 800 350-34-62',
            'phone_by_source' => [
                'avito' => '8 800 350-34-62',
                'drom' => '8 800 350-52-36',
                'exkavator' => '8 800 350-65-04',
                'gruzovik' => '7 (495) 132-38-27',
            ],
            'email' => [
                'login' => 'zrs-msk-heavyfair',
                'password' => 'lsetl8w38fnjsld',
            ],
        ],

        'USED' => [
            'key' => 'A0pSaL5yfmlcIEPspr0cYbfTxFS4DMCc',
            'url' => 'https://zeppelinused.com/',
            'phone' => '8 (800) 350-34-62',
            'phone_by_source' => [
                'avito' => '8 (800) 350-34-62',
            ],
        ],
    ];

    /**
     * C2D channel to site map
     */
    const CHANNEL_TO_SITE = [
        17998 => 'CAT',
        17999 => 'SEM',
        18000 => 'HF',
        28758 => 'USED',
    ];

    /**
     * CLM account id to site map
     */
    const ACCOUNT_TO_SITE = [
        4 => 'HF',
        5 => 'CAT',
        6 => 'SEM',
        7 => 'CAT',
        8 => 'SEM',
        9 => 'HF',
    ];

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function clsWebhook(Request $request)
    {
        foreach ($request->json()->get('data', []) as $event) {
            if ($event['type'] == 'newMessage'
                && $event['ref']['isIncoming']
                && in_array($event['ref']['sourceType'], [ 'exkavator', 'gruzovik' ])
            ) {
                try {
                    $this->processCLSMessage($event['ref']);
                }

                catch (\Exception $e) {
                    app(ExceptionHandler::class)->report($e);
                }
            }
        }
    }

    /**
     * @param array $data
     */
    protected function processCLSMessage(array $data)
    {
        app('log')->debug('[ZPL] classified message', compact('data'));

        $accountId = Arr::get($data, 'chat.account.id');

        if (!$accountId
            || !isset(self::ACCOUNT_TO_SITE[$accountId])
            || !isset($data['senderPeer'])
            || $this->isSpam($text = $data['text'])
        ) {
            return;
        }

        $site = self::ACCOUNT_TO_SITE[$accountId];
        $contacts = collect($data['senderPeer']['contacts']);
        $phone = $contacts->firstWhere('type', 'phone');
        $email = $contacts->firstWhere('type', 'email');
        $name = Arr::get($data, 'senderPeer.name') ?: Arr::get($data, 'senderPeer.sourceKey');

        if (!$phone) {
            if ($email) {
                $this->startEmail($email['value'], $name, $site, $text, $data['sourceType']);
            }

            return;
        }

        $this->postRequest(self::SITES[$site], [
            'name' => $name,
            'phone' => $phone['value'],
            'email' => $email ? $email['value'] : null,
            'text' => $text,
        ], $data['sourceType']);
    }

    /**
     * @param $email
     * @param $name
     * @param $site
     * @param $text
     * @param $source
     */
    protected function startEmail($email, $name, $site, $text, $source)
    {
        $site = self::SITES[$site];

        try {
            $transport = new \Swift_SmtpTransport('smtp.yandex.ru', 465, 'SSL');
            $transport->setUsername($site['email']['login'])->setPassword($site['email']['password']);

            $mailer = new \Swift_Mailer($transport);

            $message = (new \Swift_Message('По вашему вопросу'));
            $phone = $this->resolvePhone($site, $source);

            $message->setFrom([ "{$site['email']['login']}@yandex.ru" ])
                ->setTo([ $email => $name ])
                ->setBody(<<<EOT
Здравствуйте!

Для получения консультации, пожалуйста, позвоните по номеру {$phone}.

График работы: пн-пт, с 9 до 18.

> $text
EOT
                );

            $sent = $mailer->send($message);
        }

        catch (\Exception $e) {
            var_dump($e);
        }

//        $api = new Api('88cee8d46db0f64e1a1473012ec270', 'api.chat2desk.com');
//
//        $result = $api->post('clients', [
//            'phone' => $email,
//            'nickname' => $name,
//            'transport' => 'email',
//            'channel_id' => array_search($site, self::CHANNEL_TO_SITE),
//        ]);
//
//        $api->updateClient($result['id'], [
//            'custom_fields' => [
//                '4' => $text,
//                '5' => ucfirst($source),
//                '6' => self::SITE_TO_CUSTOM_FIELD[$site],
//            ],
//        ]);
//
//        try {
//            $api->attachTags($result['id'], 6947);
//        }
//
//        catch (ValidationException $e) {
//            //
//        }
    }

    /**
     * @param $text
     *
     * @return false|int
     */
    protected function isSpam($text)
    {
        return preg_match('/услуги|запчасти/i', $text);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function checkPhone(Request $request)
    {
        $phone = $request->json('payload.client.phone');

        $hasPhone = $request->query('hasPhone', 1436);
        $noPhone = $request->query('noPhone', 1437);

        $tunnelId = $phone && preg_match('/^79\d{9}$/', $phone) ? $hasPhone : $noPhone;

        return [
          [ 'type' => 'tunnelStart', 'payload' => compact('tunnelId') ],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function noPhoneMessage(Request $request)
    {
        $site = $this->siteByChannelId($request);
        $phone = $this->resolvePhone($site, $this->resolveSource($request));

        return [
            [
                'type' => 'updateVariables',
                'payload' => [
                    'variables' => compact('phone'),
                ],
            ]
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function addRequest(Request $request)
    {
        app('log')->info('ZEPPELIN', [ 'json' => $request->json()->all(), 'query' => $request->query->all() ]);

        $params = [
            'name' => $this->formatName($request->json('payload.client.name')),
            'phone' => $request->query('phone') ?: $request->json('payload.client.phone'),
            'text' => $request->json('payload.variables.request') ?: $request->json('payload.client.customFields.4'),
        ];

        $site = $this->siteByChannelId($request);

        try {
            $source = $this->resolveSource($request);

            $this->postRequest($site, $params, $source);
        }

        catch (\Exception $e) {
            return [ $this->systemMessage('Не удалось добавить заявку: '.$e->getMessage()) ];
        }

        return [ $this->updateVariables([ 'time' => $this->successMessage() ]) ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     *
     * @throws \Exception
     */
    protected function siteByChannelId(Request $request)
    {
        if ($site = $request->query('site')) {
            if (isset(self::SITES[$site])) {
                return self::SITES[$site];
            } else {
                throw new \Exception("Сайт {$site} не найден");
            }
        }

        $channelId = $request->json('payload.channel.externalId');

        if (!isset(self::CHANNEL_TO_SITE[$channelId])) {
            throw new HttpResponseException(new JsonResponse([
                $this->systemMessage('Для этого канала сайт не указан'),
            ]));
        }

        return self::SITES[self::CHANNEL_TO_SITE[$channelId]];
    }

    /**
     * @param array $site
     * @param array $params
     * @param $source
     *
     * @throws \Exception
     */
    protected function postRequest(array $site, array $params, $source): void
    {
        $client = new Client([
            'verify' => false,
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
            ],
        ]);

        $query = [
            'sk' => $site['key'],
            'ur' => $site['url'].'?utm_source='.$source.'&utm_term=bot',
            'la' => 'ru-RU',
        ];

        $resp = $client->get($url = 'https://tracker.comagic.ru/t/v/?'.http_build_query($query));

        $auth = \GuzzleHttp\json_decode($resp->getBody()->getContents(), true)['result'][0];

        if (!isset($auth['ci'])) {
            throw new \Exception('Не удалось авторизоваться ('.json_encode($auth).')');
        }

        $client->post('https://server.comagic.ru/api/v1/', [
            'json' => $data = [
                'name' => 'send_offline_request',
                'params' => $params,
                'site_key' => $site['key'],
                'comagic_id' => $auth['ci'],
                'hit_id' => $auth['hi'],
            ],
        ]);

        app('log')->info('[ZEPPELIN] Sent request', compact('url', 'data'));
    }

    /**
     * @param $value
     *
     * @return string
     */
    protected function formatName($value)
    {
        if (false !== $pos = mb_strpos($value, ',')) {
            return mb_substr($value, 0, $pos);
        } else {
            return $value;
        }
    }

    /**
     * @param $site
     * @param null $source
     *
     * @return mixed
     */
    protected function resolvePhone($site, $source = null)
    {
        $source = strtolower($source);

        return $source ? ($site['phone_by_source'][$source] ?? $site['phone']) : $site['phone'];
    }

    /**
     * @return string
     */
    protected function successMessage(): string
    {
        $now = Carbon::now('Europe/Moscow');
        $hour = $now->hour;
        $time = null;

        if ($now->isWeekday()) {
            if ($hour < 9) {
                $time = 'сегодня с 9 до 18';
            } elseif ($hour > 18) {
                if ($now->addDay()->isWeekday()) {
                    $time = 'завтра с 9 до 18';
                }
            } else {
                $time = 'в ближайшее время';
            }
        }

        return $time ?: 'c понедельника с 9 до 18';
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed|\Symfony\Component\HttpFoundation\ParameterBag
     */
    protected function resolveSource(Request $request)
    {
        if ($source = $request->query->get('source')) {
            return $source;
        }

        $transport = $request->json('payload.transport.name');

        if ($transport != 'external') return $transport;

        $source = $request->json('payload.client.customFields.5', 'Avito');

        return $source;
    }
}