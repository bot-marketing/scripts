<?php

namespace App\Modules\Initials;

use Illuminate\Database\Eloquent\Model;

class Initial extends Model
{
    /**
     * const
     */
    const TYPE_NAME = 1;
    const TYPE_SURNAME = 2;

    const MAN = 1;
    const WOMAN = 2;

    const SEXS = [
        0           => null,
        self::MAN   => 'male',
        self::WOMAN => 'female',
    ];

    public $timestamps = false;

    protected $fillable = [
        'type',
        'language',
        'sex',
        'people_count',
        'value'
    ];

    /**
     * @param $key
     *
     * @return mixed
     */
    public static function sexToText($key)
    {
        return self::SEXS[$key];
    }


    /**
     * @param string $text
     * @param string $type
     *
     * @return self|null
     */
    public static function relevationRaw($text, $type)
    {
        return self::select([
            'sex',
            'language',
            'people_count',
            'value',
            new \Illuminate\Database\Query\Expression('MATCH (value) AGAINST ("' . $text . '" IN NATURAL LANGUAGE MODE ) as REL')
        ])
            ->whereType($type)
//            ->where('people_count', '>', $type == self::TYPE_NAME ? 30000 : 26000)
            ->whereRaw('MATCH (`value`) AGAINST ("' . $text . '" IN NATURAL LANGUAGE MODE) > 0  ')
            ->orderByDESC('REL', 'people_count')
            ->first();
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public static function parseText(string $text): string
    {
        return trim(mb_strtolower(
                preg_replace('/ +/', ' ',
                    urldecode(
                        html_entity_decode(
                            strip_tags($text)
                        )
                    )
                ))
        );
    }

    /**
     * @param string $text
     * @param string $name
     *
     * @return string
     */
    public static function getLeftRightWords($text, $name) : string
    {
        $str = [];
        $text = explode(' ', $text);
        $key = array_search($name, $text);

        if(!$key) return implode(' ', $text);

        $str[] = $text[$key - 1] ?? '';
        $str[] = $text[$key + 1] ?? '';

        return implode(' ', $str);
    }
}