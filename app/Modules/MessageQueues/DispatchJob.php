<?php

namespace App\Modules\MessageQueues;

use App\Services\BotCorp\ApiClient;
use GuzzleHttp\Exception\TransferException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DispatchJob implements ShouldQueue
{
    use SerializesModels, InteractsWithQueue, Queueable;

    /**
     * @var \App\Modules\MessageQueues\MessageQueue
     */
    protected $model;

    /**
     * DispatchJob constructor.
     *
     * @param \App\Modules\MessageQueues\MessageQueue $queue
     */
    public function __construct(MessageQueue $queue)
    {
        $this->model = $queue;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $queue = $this->model;

        if ($queue->status != MessageQueue::S_DISPATCHING) return;

        if ($queue->time->addMinutes(30)->isPast()) {
            $queue->status = MessageQueue::S_EXPIRED;
            $queue->save();

            return;
        }

        try {
            $this->send();

            $queue->status = MessageQueue::S_SENT;
        }

        catch (TransferException $e) {
            dispatch((new self($queue))->delay(10));

            return;
        }

        catch (\Exception $e) {
            app(ExceptionHandler::class)->report($e);

            $queue->status = MessageQueue::S_FAILED;
            $queue->error = $e->getMessage();
        }

        $queue->save();
    }

    /**
     * Send api request.
     */
    protected function send()
    {
        $queue = $this->model;

        $api = new ApiClient($queue->provider_auth);

        $phones = $queue->contacts();

        switch (count($phones)) {
            case 0:
                break;

            case 1:
                $api->get('whatsapp_send', [
                    'text' => $queue->text,
                    'file_url' => $queue->file_url,
                    'phone' => $phones[0],
                ]);

                break;

            default:
                $api->get('whatsapp_mailing', [
                    'name' => $queue->name,
                    'text' => $queue->text,
                    'file_url' => $queue->file_url,
                    'filter' => compact('phones'),
                ]);

                break;
        }
    }
}