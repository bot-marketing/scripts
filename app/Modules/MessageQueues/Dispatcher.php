<?php

namespace App\Modules\MessageQueues;

class Dispatcher
{
    /**
     * Dispatch queue.
     */
    public function dispatch()
    {
        foreach (MessageQueue::pending() as $queue) {
            $this->dispatchQueue($queue);
        }
    }

    /**
     * @param \App\Modules\MessageQueues\MessageQueue $queue
     */
    public function dispatchQueue(MessageQueue $queue)
    {
        $queue->status = MessageQueue::S_DISPATCHING;
        $queue->save();

        dispatch(new DispatchJob($queue));
    }
}