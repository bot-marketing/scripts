<?php

namespace App\Modules\MessageQueues;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MessageQueue
 *
 * @package App\Modules\MessageQueues
 *
 * @property int $id
 * @property string $hash
 * @property int $provider
 * @property string $provider_auth
 * @property string|null $text
 * @property string|null $file_url
 * @property \Carbon\Carbon $time
 * @property int $status
 * @property string $name
 * @property string $error
 */
class MessageQueue extends Model
{
    const P_BOTCORPIO = 1;

    const S_PENDING = 0;
    const S_DISPATCHING = 1;
    const S_SENT = 2;
    const S_FAILED = -1;
    const S_EXPIRED = -2;

    /**
     * @var string
     */
    protected $table = 'message_queues';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = [ 'time' ];

    /**
     * @param int $provider
     * @param string $providerAuth
     * @param string $name
     * @param string|null $text
     * @param string|null $fileUrl
     * @param \Carbon\Carbon $time
     *
     * @return \App\Modules\MessageQueues\MessageQueue
     */
    public static function findOrCreate(
        int $provider, string $providerAuth, string $name, ?string $text, ?string $fileUrl, Carbon $time
    ): self {
        $time = $time->copy()->setTimezone('UTC');

        $hash = md5(implode(':', [ $provider, $providerAuth, $name, $text, $fileUrl, $time ]));

        if ($model = self::query()->where('hash', $hash)->first()) {
            return $model;
        }

        $model = new self;

        $model->hash = $hash;
        $model->provider = $provider;
        $model->provider_auth = $providerAuth;
        $model->name = $name;
        $model->text = $text;
        $model->file_url = $fileUrl;
        $model->time = $time;
        $model->status = self::S_PENDING;

        $model->save();

        return $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function pending()
    {
        return self::query()->where('status', self::S_PENDING)->where('time', '<=', Carbon::now())->get();
    }

    /**
     * @param $phone
     *
     * @return bool
     */
    public function pushContact($phone): bool
    {
        try {
            $this->getConnection()->table('message_queues_contacts')->insert([
                'queue_id' => $this->id,
                'phone' => $phone,
            ]);

            return true;
        }

        catch (\PDOException $e) {
            return false;
        }
    }

    /**
     * @return array
     */
    public function contacts(): array
    {
        return $this->getConnection()
            ->table('message_queues_contacts')
            ->where('queue_id', $this->id)
            ->pluck('phone')
            ->all();
    }
}