<?php

namespace App\Providers;

use Google\Client;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('guzzle.handler', function () {
            return HandlerStack::create();
        });

        $this->app->singleton(\Google_Client::class, function ($app, $params) {
            $client = new Client([
                'retry' => [ 'retries' => 2 ],
            ]);
            $credentials = $params[0] ?? null;
            if (is_string($credentials)) {
                if (!file_exists($credentials = storage_path('app/'.$credentials.'.json'))) {
                    throw new \RuntimeException('Auth file does not exists');
                }
            }
            $client->setAuthConfig($credentials ?? base_path('ASMO-ce2db0e2e708.json'));
            $client->addScope(\Google_Service_Sheets::SPREADSHEETS);
            $client->setAccessType('offline');
            $client->setHttpClient(new \GuzzleHttp\Client([
                'handler' => app('guzzle.handler'),
                'base_uri' => Client::API_BASE_PATH,
                'http_errors' => false,
                'connect_timeout' => 3,
                'timeout' => 20,
            ]));

            return $client;
        });

        $this->app->singleton(\Google_Service_Sheets::class, function () {
            return new \Google_Service_Sheets($this->app->make(\Google_Client::class));
        });
    }
}
