<?php

namespace App\Services\Adcome;


use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;


class ApiClient
{

    /**
     * @var string
     */
    const KEY = 'Adcome_CACHE';

    /**
     * @var string
     */
    const DOMAIN = 'https://api.adcome.ru/';

    /**
     * @var string
     */
    const METHOD_PROMOCODE = 'getpromocode/mfk/3429414104/4932124';

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * ApiClient constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri'    => self::DOMAIN,
            'verify'      => false,
            'http_errors' => false,
        ]);
    }

    /**
     * @param       $method
     * @param array $data
     *
     * @return string
     * @throws \Exception
     */
    public function get($method, $data = []): string
    {
        $url = self::DOMAIN . $method;
        return $this->accessRestrictions(function () use ($url, $data) {
            return $this->parseResponse($this->client->get($url, [
                'form_params' => $data,
            ]));
        });
    }


    /**
     * @param $function
     *
     * @return mixed
     */
    public static function accessRestrictions($function)
    {
        return app('redis')
            ->funnel(self::KEY)
            ->limit(1)
            ->block(2)
            ->then($function);
    }


    /**
     * @param ResponseInterface $response
     *
     * @return string
     */
    protected function parseResponse(ResponseInterface $response): string
    {
        $data = $response->getBody()->getContents();

        $response->getBody()->close();

        return trim(strip_tags($data));
    }
}