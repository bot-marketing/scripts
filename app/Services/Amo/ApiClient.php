<?php

namespace App\Services\Amo;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;

class ApiClient
{
    /**
     * @var string
     */
    protected $domain;

    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $hash;

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * @var \GuzzleHttp\Cookie\CookieJar
     */
    private $cookies;

    /**
     * @var \Illuminate\Cache\Repository
     */
    protected $cache;

    /**
     * ApiClient constructor.
     *
     * @param $domain
     * @param $login
     * @param $hash
     */
    public function __construct($domain, $login, $hash)
    {
        $this->cache = app('cache')->store();
        $this->domain = $domain;
        $this->login = $login;
        $this->hash = $hash;

        $auth = false;

        if (!$this->cookies = $this->makeCookieJar()) {
            $this->cookies = new CookieJar();
            $auth = true;
        }

        $this->client = new Client([
            'base_uri' => ($domain = rtrim($domain, '/')).'/api/',
            'cookies' => $this->cookies,
            'verify' => false,
            'connect_timeout' => 10,
            'http_errors' => false,
            'headers' => [ 'Accept' => 'application/json' ],
        ]);

        if ($auth) $this->auth();
    }

    /**
     * Authenticate and store cookies.
     *
     * @throws \App\Services\Amo\ApiException
     */
    private function auth()
    {
        $response = $this->client->post($this->domain.'/private/api/auth.php', [
            'form_params' => [ 'USER_LOGIN' => $this->login, 'USER_HASH' => $this->hash ],
            'query' => [ 'type' => 'json' ],
            'http_errors' => false,
        ]);

        if (($code = $response->getStatusCode()) == 401) {
            throw new ApiException('Failed to authenticate');
        }

        if ($code >= 400) {
            throw new ApiException('Http Error '.$code, $code);
        }

        $response->getBody()->close();

        $this->cache->put($this->cacheKey(), $this->cookies->toArray(), Carbon::now()->addHour());
    }

    /**
     * @param $method
     * @param array $query
     *
     * @return mixed|null
     */
    public function get($method, $query = [])
    {
        return $this->request('get', $method, compact('query'));
    }

    /**
     * @param $method
     * @param $json
     *
     * @return mixed|null
     */
    public function post($method, $json)
    {
        return $this->request('post', $method, compact('json'));
    }

    /**
     * @param $method
     * @param $url
     * @param $params
     *
     * @return mixed
     *
     * @throws \App\Services\Amo\ApiException
     */
    protected function request($method, $url, $params)
    {
        /**
         * @var ResponseInterface $response
         */
        try {
            return $this->processResponse($this->client->$method($url, $params));
        }

        catch (ApiException $e) {
            if ($e->getCode() == 401) {
                $this->auth();

                return $this->processResponse($this->client->$method($url, $params));
            } else {
                throw $e;
            }
        }
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface $response
     *
     * @return mixed|null
     * @throws \App\Services\Amo\ApiException
     */
    protected function processResponse(ResponseInterface $response)
    {
        try {
            if (($code = $response->getStatusCode()) >= 500) {
                throw new ApiException('Http Exception '.$code, $code);
            }

            if ($code === 204) return null;

            $result = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

            if (isset($result['response']['error'])) {
                throw new ApiException($result['response']['error'].' ('.$result['response']['error_code'].')', $code);
            }

            if ($code >= 400) throw new ApiException('Http Exception '.$code, $code);

            return $result;
        }

        finally {
            $response->getBody()->close();
        }
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @return \GuzzleHttp\Cookie\CookieJar|null
     */
    protected function makeCookieJar(): ?CookieJar
    {
        if ($data = $this->cache->get($this->cacheKey())) {
            return new CookieJar(false, $data);
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    protected function cacheKey()
    {
        return md5("amo:{$this->domain}:{$this->login}:{$this->hash}");
    }
}