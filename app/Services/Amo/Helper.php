<?php

namespace App\Services\Amo;

use Carbon\Carbon;
use Illuminate\Support\Arr;

class Helper
{
    /**
     * @var \App\Services\Amo\ApiClient
     */
    protected $api;

    /**
     * @var \Illuminate\Cache\Repository
     */
    protected $cache;

    /**
     * Helper constructor.
     *
     * @param \App\Services\Amo\ApiClient $api
     */
    public function __construct(ApiClient $api)
    {
        $this->api = $api;
        $this->cache = app('cache')->store();
    }

    /**
     * @param array $client
     *
     * @return int|null
     */
    public function findLeadIdForClient(array $client)
    {
        $key = md5("amo:{$this->api->getDomain()}:{$client['externalId']}:{$client['phone']}");

        if ($id = $this->cache->get($key)) return $id;

        if (($contact = $this->findContact($client)) && ($ids = $contact['leads']['id'] ?? null)) {
            if (!$leads = $this->getLeads($ids)) return null;

            $openedLead = Arr::first($leads, function ($data) {
                return $data['closed_at'] == 0;
            });

            $id = $openedLead['id'] ?? null;

            $this->cache->put($key, $id, Carbon::now()->addMinutes(10));

            return $id;
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @param array $payload
     *
     * @return mixed
     */
    public function makeContact($data, $payload = [])
    {
        $key = "(client_id_{$data['externalId']})";
        $name = $payload['name'] ?? ($data['name'] ?: $data['externalId']);

        $payload['name'] = "$name $key";

        if ($result = $this->findContact($data)) {
            return $result;
        }  else {
            return $this->addItem('contacts', $payload);
        }
    }

    /**
     * @param array $client
     *
     * @return mixed|null
     */
    public function findContact(array $client)
    {
        if ($result = $this->getItem('v2/contacts', [ 'query' => "(client_id_{$client['externalId']})" ])) {
            return $result;
        } elseif ($result = $this->getItem('v2/contacts', [ 'query' => "({$client['externalId']}-" ])) {
            return $result;
        } elseif ($result = $this->findContactByPhone($client['phone'])) {
            return $result;
        } else {
            return null;
        }
    }

    /**
     * @param $method
     * @param array $params
     *
     * @return mixed
     */
    public function getItem($method, $params = [])
    {
        return $this->item($this->api->get($method, $params));
    }

    /**
     * @param $value
     *
     * @return mixed|null
     */
    public function findContactByPhone($value)
    {
        $phone = $this->processPhone($value);

        if ($phone && ($result = $this->api->get('v2/contacts', [ 'query' => $phone ]))) {
            return $this->item($result);
        } else {
            return null;
        }
    }

    /**
     * @param $value
     *
     * @return string|null
     */
    protected function processPhone($value)
    {
        if (!is_numeric($value)) return $value;

        if (preg_match('/^7(\d{10})$/', $value, $matches)) return $matches[1];

        return null;
    }

    /**
     * @param $contactId
     * @param array $data
     *
     * @return mixed
     */
    public function addLead($contactId, $data = [])
    {
        $data['contacts_id'] = $contactId;

        return $this->addItem('leads', $data);
    }

    /**
     * @param $elementType
     * @param $elementId
     * @param $text
     *
     * @param int $type
     *
     * @return array
     */
    public function addNote($elementType, $elementId, $text, $type = 4)
    {
        return $this->addItem('notes', [
            'element_type' => $elementType,
            'element_id' => $elementId,
            'note_type' => $type,
            'text' => $text,
        ]);
    }

    /**
     * @param $id
     * @param $text
     *
     * @param int $type
     *
     * @return array
     */
    public function addNoteToLead($id, $text, $type = 4)
    {
        return $this->addNote(2, $id, $text, $type);
    }

    /**
     * @param $resource
     * @param $data
     *
     * @return mixed
     */
    protected function addItem($resource, $data)
    {
        return $this->item($this->api->post('v2/'.$resource, [ 'add' => [ $data ] ]));
    }

    /**
     * @param $resource
     * @param $id
     * @param $data
     *
     * @return mixed
     */
    public function updateItem($resource, $id, $data)
    {
        return $this->item($this->api->post('v2/'.$resource, [ 'update' => [
            [ 'id' => $id, 'updated_at' => time() ] + $data ],
        ]));
    }

    /**
     * @param $id
     * @param $data
     *
     * @return mixed
     */
    public function updateLead($id, $data)
    {
        return $this->updateItem('leads', $id, $data);
    }

    /**
     * @param $id
     * @param $data
     *
     * @return mixed
     */
    public function updateContact($id, $data)
    {
        return $this->updateItem('contacts', $id, $data);
    }

    /**
     * @param $response
     *
     * @return mixed
     */
    protected function item($response)
    {
        return $response['_embedded']['items'][0] ?? null;
    }

    /**
     * @param $response
     *
     * @return mixed|null
     */
    protected function items($response)
    {
        return $response['_embedded']['items'] ?? null;
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function urlToLead($id)
    {
        return "{$this->api->getDomain()}/leads/detail/$id";
    }

    /**
     * @param $ids
     *
     * @return mixed|null
     */
    protected function getLeads($ids)
    {
        return $this->items($this->api->get('v2/leads', [ 'id' => $ids ]));
    }
}