<?php

namespace App\Services\Bibicol;

use GuzzleHttp\Client;
use Hamcrest\Thingy;
use Psr\Http\Message\ResponseInterface;

class ApiClient
{
//    const AMP_API_KEY = '8bcb7b17-ef2c-4cd0-9fe9-b7c4b414605d'; // ключ для AMP Check

    protected $apiKey;
    protected $url;
    protected $contentType;

    /**
     * ApiClient constructor.
     *
     * @param $apiKey
     * @param $url
     * @param $contentType
     */

    public function __construct($apiKey, $url, $contentType)
    {
        $this->client = new Client([
            'base_uri' => $this->url = $url,
            'connect_timeout' => 10,
            'headers' => [
                'Content-type' => $this->contentType = $contentType,
                'api-key' => $this->apiKey = $apiKey
            ],
        ]);
    }

    public function get($method, $query = [])
    {
        return $this->parseResponse($this->client->get($method, compact('query')));
    }

    public function post($method, $params)
    {
        return $this->parseResponse($this->client->post($method, ['form_params' => $params]));
    }

    public function postJson($method, $params)
    {
        return $this->parseResponse($this->client->post($method, ['json' => $params]));
    }

    public function readQrCode($params)
    {
        return $this->post('read-qr-code/', $params);
    }

    public function postQrReceipt($params)
    {
        return $this->postJson('receipts/qr', $params);
    }

    public function receipt($query)
    {
        return $this->get('receipts/' . $query);
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface $response
     *
     * @return mixed
     * @throws \App\Services\Bibicol\ApiException
     */

    protected function parseResponse(ResponseInterface $response)
    {

        $data = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

        $response->getBody()->close();

        if (isset($data['symbol']['error'])) {
            throw new ApiException($data['symbol']['error']);
        }

        return $data;
    }
}