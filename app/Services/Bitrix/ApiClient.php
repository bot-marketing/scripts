<?php

namespace App\Services\Bitrix;

use GuzzleHttp\Client;

class ApiClient
{
    /**
     * @var string
     */
    protected $domain;

    /**
     * ApiClient constructor.
     *
     * @param $hookUrl
     */
    public function __construct($hookUrl)
    {
        $this->httpClient = new Client([
            'base_uri' => $hookUrl,
            'verify' => false,
            'http_errors' => false,
        ]);

        $this->domain = parse_url($hookUrl, PHP_URL_HOST);
    }

    /**
     * @param $resource
     * @param $id
     *
     * @return string
     */
    public function urlToItem($resource, $id)
    {
        return "https://{$this->domain}/crm/$resource/details/$id/";
    }

    /**
     * @param array $requests
     * @param int $halt
     *
     * @return mixed
     */
    public function batch(array $requests, $halt = 1)
    {
        return $this->request('batch', [
            'halt' => $halt,
            'cmd' => array_map([$this, 'makeBatchRequest'], $requests),
        ]);
    }

    /**
     * @param array $request
     *
     * @return string
     */
    protected function makeBatchRequest(array $request)
    {
        $url = $request['method'];

        return isset($request['query']) ? $url.'?'.http_build_query($request['query']) : $url;
    }

    /**
     * @param $method
     * @param array $query
     *
     * @return mixed
     *
     * @throws \App\Services\Bitrix\ApiException
     */
    public function request($method, $query = [])
    {
        try {
            $response = $this->httpClient->post("$method.json", [ 'form_params' => $query ]);

            if ($response->getStatusCode() >= 500) {
                throw new ApiException('SERVER_ERROR_'.$response->getStatusCode());
            }

            $data = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

            if (isset($data['error'])) {
                throw new ApiException($data['error'] ?? $data['error_description'] ?? 'Unknown error');
            }

            return $data['result'];
        }

        finally {
            if (isset($response)) $response->getBody()->close();
        }
    }
}