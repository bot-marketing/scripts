<?php

namespace App\Services\Bitrix;

use Illuminate\Http\Request;

class Helper
{
    const ORIGINATOR = 'BotMarketing';

    /**
     * @var \App\Services\Bitrix\ApiClient
     */
    protected $api;

    /**
     * Helper constructor.
     *
     * @param \App\Services\Bitrix\ApiClient $api
     */
    public function __construct(ApiClient $api)
    {
        $this->api = $api;
    }

    /**
     * @param array $client
     *
     * @return int|null
     */
    public function findLead(array $client)
    {
        $existingClients = $this->api->request('crm.lead.list', [
            'filter' => [
                'ORIGINATOR_ID' => self::ORIGINATOR,
                'ORIGIN_ID' => $client['externalId'],
                'DATE_CLOSED' => 'NULL',
            ],
            'select' => [ 'ID' ],
        ]);

        if ($existingClients) return $existingClients[0]['ID'];

        if (!is_numeric($client['phone'])) return null;

        $existingClients = $this->api->request('crm.lead.list', [
            'filter' => [
                'PHONE' => $client['phone'],
                'DATE_CLOSED' => 'NULL',
            ],
            'order' => [ 'DATE_CREATE' => 'DESC' ],
            'select' => [ 'ID' ],
        ]);

        if (!$existingClients) return null;

        $this->api->request('crm.lead.update', [
            'id' => $existingClients[0]['ID'],

            'fields' => [
                'ORIGINATOR_ID' => self::ORIGINATOR,
                'ORIGIN_ID' => $client['externalId'],
            ],
        ]);

        return $existingClients[0]['ID'];
    }

    /**
     * @param array $client
     * @param array $params
     *
     * @return mixed
     */
    public function addLead(array $client, array $params = [])
    {
        $phoneData = [];

        if (is_numeric($client['phone'])) {
            $phoneData['PHONE'] = [
                [
                    'VALUE' => $client['phone'],
                    'VALUE_TYPE' => 'WORK',
                    'TYPE_ID' => 'PHONE',
                ],
            ];
        }

        return $this->api->request('crm.lead.add', [
            'fields' => array_merge([
                'NAME' => $client['name'],
                'OPENED' => 'Y',
                'ORIGINATOR_ID' => self::ORIGINATOR,
                'ORIGIN_ID' => $client['externalId'],
            ], $phoneData, $params),
        ]);
    }

    /**
     * @param $id
     * @param array $data
     *
     * @return mixed
     */
    public function updateLead($id, array $data)
    {
        return $this->api->request('crm.lead.update', [
            'id' => $id,
            'fields' => $data,
        ]);
    }
}