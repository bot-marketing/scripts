<?php

namespace App\Services\Bizon365;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class ApiClient
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $token;

    /**
     * ApiClient constructor.
     *
     * @param $token
     */

    public function __construct($token)
    {
        $this->client = new Client([
            'base_uri' => 'https://online.bizon365.ru/api/v1/',
            'connect_timeout' => 10,
            'headers' => [
                'Content-type' => 'application/x-www-form-urlencoded',
                'X-Token' => $this->token = $token,
            ],
            'verify' => false,
            'http_errors' => false,
        ]);
    }

    // обработка Get запроса
    public function get($method, $query = [])
    {
        return $this->accessRestrictions(function () use ($method, $query) {
            return $this->parseResponse($this->client->get($method, compact('query')));
        });
    }

    // обработка POST запроса
    public function post($method, $params)
    {
        return $this->accessRestrictions(function () use ($method, $params) {
            return $this->parseResponse($this->client->post($method, [ 'form_params' => $params ]));
        });
    }

    public function subpages()
    {
        return $this->get('webinars/subpages/getSubpages');
    }

    public function addSubscriber($params)
    {
        return $this->post('webinars/subpages/addSubscriber', $params);
    }

    public function allSubscribers($query = [])
    {
        return $this->get('webinars/subpages/getSubscribers?', $query);
    }

    public function accessRestrictions($function)
    {
        return app('redis')
            ->funnel("bison:{$this->token}")
            ->limit(1)
            ->block(30)
            ->then($function);
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface $response
     *
     * @return mixed
     * @throws \App\Services\Bizon365\ApiException
     */
    protected function parseResponse(ResponseInterface $response)
    {
        $data = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

        $response->getBody()->close();

        if (isset($data['message'])) {
            throw new ApiException($data['message']);
        }

        return $data;
    }
}