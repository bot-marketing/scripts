<?php

namespace App\Services\BotCorp;

use GuzzleHttp\Client;

class ApiClient
{
    /**
     * @var string
     */
    protected $token;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * ApiClient constructor.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
        $this->client = new Client([
            'handler' => app('guzzle.handler'),
            'base_uri' => 'https://sendapi.net/api.php',
            'verify' => false,
        ]);
    }

    /**
     * @param $method
     * @param array $params
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function get($method, $params = [])
    {
        $params['method'] = $method;
        $params['api_id'] = $this->token;

        $result = $this->client->post('', [
            'form_params' => [
                'json_data' => json_encode(array_filter($params)),
            ],
        ]);

        $result = json_decode($result->getBody()->getContents(), true);

        if ($result['result'] > 0) {
            throw new ApiException($result['result_description']);
        }

        return $result['result_data'] ?? true;
    }
}