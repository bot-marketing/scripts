<?php

namespace App\Services\Chat2Desk;

use App\Services\Chat2Desk\DataTypes\DataType;
use App\Services\Chat2Desk\DataTypes\Dialog;
use App\Services\Chat2Desk\Requests\SendMessage;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;

class Api
{
    const SKIP_ERROR_NOTIFICATION = [
        'not found for client',
        'API calls exceeded API limit',
        'Rename this chat',
        'Operator not found!',
    ];

    /**
     * @var string
     */
    protected $token;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $domain;

    /**
     * Api constructor.
     *
     * 57c8b37a7a99173add33a85e8d6f05
     *
     * @param $token
     * @param null $domain
     */
    public function __construct($token = null, $domain = null)
    {
        $this->token = $token;
        $this->domain = $domain;
    }

    /**
     * @param SendMessage $message
     *
     * @return DataTypes\MessageSendResult
     */
    public function sendMessage(SendMessage $message)
    {
        $response = $this->post('messages', $message->toArray());

        return new DataTypes\MessageSendResult($response);
    }

    /**
     * @param $clientId
     * @param $idList
     *
     * @return bool
     */
    public function attachTags($clientId, $idList)
    {
        return $this->post('tags/assign_to', [
            'assignee_type' => 'client',
            'assignee_id' => $clientId,
            'tag_ids' => $idList,
        ]);
    }

    /**
     * @param $clientId
     * @param $tagId
     *
     * @return mixed
     */
    public function detachTag($clientId, $tagId)
    {
        return $this->delete("tags/{$tagId}/delete_from", [
            'client_id' => $clientId,
        ]);
    }

    /**
     * @return DataTypes\ApiInfo
     */
    public function info()
    {
        return $this->getObject(DataTypes\ApiInfo::class, 'companies/api_info');
    }

    /**
     * @param $id
     *
     * @return Dialog
     */
    public function dialog($id)
    {
        return $this->getObject(DataTypes\Dialog::class, "dialogs/$id");
    }

    /**
     * @param $id
     * @param $operatorId
     *
     * @return bool
     */
    public function updateDialog($id, $operatorId, $state)
    {
        return $this->put("dialogs/$id", [
            'operator_id' => $operatorId,
            'state' => $state,
        ]);
    }

    /**
     * @return DataTypes\Transport[]|Collection
     */
    public function transports()
    {
        return collect($this->get('help/transports'))
            ->map(function ($value, $key) {
                return DataTypes\Transport::make($key, $value);
            })
            ->values();
    }

    /**
     * @param array $query
     *
     * @return Collection
     */
    public function clients($query = [])
    {
        return collect($this->get('clients', $query));
    }

    /**
     * @param $clientId
     *
     * @return Collection
     */
    public function clientTransports($clientId)
    {
        return collect($this->get("clients/$clientId/transports"));
    }

    /**
     * @param $clientId
     *
     * @return \stdClass
     */
    public function clientInfo($clientId)
    {
        return $this->get("clients/$clientId");
    }

    /**
     * @param $url
     * @param null $events
     *
     * @return bool
     */
    public function setWebHook($url, $events = null)
    {
        return $this->post('companies/web_hook', compact('url', 'events'));
    }

    /**
     * @param $url
     *
     * @return bool
     */
    public function setChatCloseWebHook($url)
    {
        return $this->post('companies/chat_close_web_hook', compact('url'));
    }

    /**
     * @param $id
     * @param array $fields
     *
     * @return mixed
     */
    public function updateClient($id, array $fields)
    {
        return $this->put("clients/{$id}", $fields);
    }

    /**
     * @param $id
     *
     * @return DataTypes\Message
     */
    public function message($id)
    {
        return $this->getObject(DataTypes\Message::class, "messages/$id");
    }

    /**
     * @param $id
     * @param $groupId
     *
     * @return array
     */
    public function messageTransferToGroup($id, $groupId)
    {
        return $this->get("messages/$id/transfer_to_group", [
            'group_id' => $groupId,
        ]);
    }

    /**
     * @param $id
     * @param $operatorId
     *
     * @return mixed
     */
    public function messageAssignToOperator($id, $operatorId)
    {
        return $this->post("messages/$id/assign", [ 'operator_id' => $operatorId ]);
    }

    /**
     * @param $id
     * @param $operatorId
     *
     * @return mixed
     */
    public function messageTransferToOperator($id, $operatorId)
    {
        return $this->get("messages/$id/transfer", [ 'operator_id' => $operatorId ]);
    }

    /**
     * @param $label
     * @param $description
     * @param $groupId
     *
     * @return mixed
     */
    public function createTag($label, $description, $groupId)
    {
        return $this->post('tags', [
            'tag_group_id' => $groupId,
            'tag_label' => $label,
            'tag_description' => $description,
            'tag_bg_color' => '008bfb',
            'tag_text_color' => 'ffffff',
        ]);
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function createTagGroup($name)
    {
        return $this->post('tag_groups', [
            'tag_group_name' => $name,
            'display' => 1,
        ]);
    }

    /**
     * @param $className
     * @param $path
     * @param array $query
     *
     * @return mixed
     */
    protected function getObject($className, $path, array $query = [])
    {
        return new $className($this->get($path, $query));
    }

    /**
     * @param $path
     * @param array $query
     *
     * @return mixed
     */
    public function get($path, array $query = [])
    {
        return $this->call('get', [ $path, compact('query') ]);
    }

    /**
     * @param $path
     * @param $json
     *
     * @return mixed
     */
    public function post($path, $json)
    {
        return $this->call('post', [ $path, compact('json') ]);
    }

    /**
     * @param $path
     * @param $json
     *
     * @return mixed
     */
    public function put($path, $json)
    {
        return $this->call('put', [ $path, compact('json') ]);
    }

    /**
     * @param $path
     *
     * @return mixed
     */
    public function delete($path, $json = [])
    {
        return $this->call('delete', [ $path, compact('json') ]);
    }

    /**
     * @param $method
     * @param $arguments
     *
     * @return mixed
     *
     * @throws ConnectException
     * @throws ErrorException
     */
    protected function call($method, $arguments)
    {
        try {
            $response = call_user_func_array([ $this->getClient(), $method ], $arguments);

            return $this->parseResponse($response);
        }

        catch (\GuzzleHttp\Exception\ConnectException $e) {
            throw new ConnectException('Failed to connect to the server.', 0, $e);
        }
    }

    /**
     * @param ResponseInterface $response
     *
     * @return mixed
     *
     * @throws ErrorException
     */
    protected function parseResponse(ResponseInterface $response)
    {
        try {
            if (($code = $response->getStatusCode()) >= 500) {
                throw new ServerErrorException('Unexpected server error.', $code);
            }

            $data = \GuzzleHttp\json_decode($response->getBody()->getContents());

            if (isset($data->error)) {
                throw new ClientErrorException($data->error);
            }

            if ($data->status == 'error' || is_numeric($data->status)) {
                $message = $data->message ?? 'Unknown error';

                if (isset($data->errors)) {
                    throw new ValidationException(new MessageBag((array)$data->errors));
                }

                throw new ClientErrorException($message, $response->getStatusCode());
            }

            return isset($data->data) ? $data->data : true;
        }

        finally {
            $response->getBody()->close();
        }
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        if ($this->client) {
            return $this->client;
        }

        return $this->client = new Client([
            'base_uri' => "https://{$this->domain}/v1/",
            'headers' => [
                'Authorization' => $this->token,
                'Accept' => 'application/json',
            ],
            'verify' => false,
            'http_errors' => false,
            'timeout' => 60,
        ]);
    }

    /**
     * @param $method
     * @param $arguments
     * @param \Exception $e
     */
    protected function notifyFailedRequest($method, $arguments, $e)
    {
        if ($e instanceof ValidationException
            || Str::contains($e->getMessage(), self::SKIP_ERROR_NOTIFICATION)
        ) {
            return;
        }
    }
}