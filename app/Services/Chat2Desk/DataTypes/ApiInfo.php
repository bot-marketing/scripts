<?php

namespace App\Services\Chat2Desk\DataTypes;

class ApiInfo extends DataType
{
    /**
     * @return string
     */
    public function getApiMode()
    {
        return $this->data->mode;
    }

    /**
     * @return string
     */
    public function getCompanyMode()
    {
        return $this->data->company_mode;
    }

    /**
     * @return string
     */
    public function getAdminEmail()
    {
        return $this->data->admin_email;
    }

    /**
     * @return string
     */
    public function getHook()
    {
        return $this->data->hook;
    }

    /**
     * @return int
     */
    public function getCurrentVersion()
    {
        return $this->data->current_version;
    }

    /**
     * @return int
     */
    public function getRequestsThisMonth()
    {
        return $this->data->requests_this_month;
    }

    /**
     * @return string
     */
    public function getChatCloseWebHook()
    {
        return $this->data->chat_close_web_hook;
    }

    /**
     * The number of channels.
     *
     * @return int
     */
    public function getChannels()
    {
        return $this->data->channels;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->data->company_name;
    }

    /**
     * @return bool
     */
    public function isDemo()
    {
        return $this->getCompanyMode() == 'demo';
    }

    /**
     * @return bool
     */
    public function isApiLimited()
    {
        return $this->getApiMode() == 'demo';
    }
}