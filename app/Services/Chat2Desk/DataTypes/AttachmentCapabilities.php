<?php

namespace App\Services\Chat2Desk\DataTypes;

class AttachmentCapabilities extends DataType
{
    const TEXT = 1;
    const IMAGE = 2;
    const AUDIO = 4;
    const VIDEO = 8;
    const PDF = 16;
    const LOCATION = 32;

    /**
     * @inheritdoc
     */
    protected function processData()
    {
        foreach ($this->data as $key => &$value) {
            $value = $value == 'yes' ? true : false;
        }
    }

    /**
     * @return bool
     */
    public function supportsText()
    {
        return $this->data->text;
    }

    /**
     * @return bool
     */
    public function supportsImage()
    {
        return $this->data->image;
    }

    /**
     * @return bool
     */
    public function supportsAudio()
    {
        return $this->data->audio;
    }

    /**
     * @return bool
     */
    public function supportsVideo()
    {
        return $this->data->video;
    }

    /**
     * @return bool
     */
    public function supportsPdf()
    {
        return $this->data->pdf;
    }

    /**
     * @return bool
     */
    public function supportsLocation()
    {
        return $this->data->location;
    }

    /**
     * @return int
     */
    public function toInt()
    {
        $bits = [
            [ self::TEXT, 'text' ],
            [ self::IMAGE, 'image' ],
            [ self::AUDIO, 'audio' ],
            [ self::VIDEO, 'video' ],
            [ self::PDF, 'pdf' ],
            [ self::LOCATION, 'location' ],
        ];

        return collect($bits)->reduce(function ($result, $value) {
            list($bit, $key) = $value;

            return $this->data->$key ? ($result | $bit) : $result;
        }, 0);
    }
}