<?php

namespace App\Services\Chat2Desk\DataTypes;

use Illuminate\Contracts\Support\Arrayable;

class DataType implements Arrayable
{
    /**
     * @var \stdClass
     */
    protected $data;

    /**
     * DataType constructor.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;

        $this->processData();
    }

    /**
     * Do some stuff after data is set.
     */
    protected function processData()
    {
        //
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return (array)$this->data;
    }
}