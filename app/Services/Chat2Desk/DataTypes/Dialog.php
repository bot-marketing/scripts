<?php

namespace App\Services\Chat2Desk\DataTypes;

class Dialog extends DataType
{
    const S_CLOSED = 'closed';
    const S_OPENED = 'open';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->data->id;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->data->state;
    }

    /**
     * @return string
     */
    public function getOperatorId()
    {
        return $this->data->operator_id;
    }

    /**
     * @return bool
     */
    public function isClosed()
    {
        return $this->getState() == self::S_CLOSED;
    }

    /**
     * @return bool
     */
    public function isOpened()
    {
        return $this->getState() == self::S_OPENED;
    }
}