<?php

namespace App\Services\Chat2Desk\DataTypes;

class Message extends DataType
{
    public function getId()
    {
        return $this->data->id;
    }

    public function getText()
    {
        return $this->data->text;
    }

    public function getPhoto()
    {
        return $this->data->photo;
    }

    public function getDialogId()
    {
        return $this->data->dialog_id;
    }
}