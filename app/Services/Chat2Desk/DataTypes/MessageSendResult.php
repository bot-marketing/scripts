<?php

namespace App\Services\Chat2Desk\DataTypes;

use App\Accounts\User;
use App\Gate\DataTypes\GateMeta;

class MessageSendResult extends DataType
{
    /**
     * @return int
     */
    public function getMessageId()
    {
        return $this->data->message_id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->data->type;
    }

    /**
     * @return int
     */
    public function getClientId()
    {
        return $this->data->client_id;
    }

    /**
     * @return int
     */
    public function getTransport()
    {
        return $this->data->transport;
    }

    /**
     * @return int
     */
    public function getChannelId()
    {
        return $this->data->channel_id;
    }

    /**
     * @return int
     */
    public function getOperatorId()
    {
        return $this->data->operator_id;
    }

    /**
     * @return int
     */
    public function getDialogId()
    {
        return $this->data->dialog_id;
    }

    /**
     * @param User $user
     *
     * @return GateMeta|null
     */
    public function resolveGateMeta(User $user)
    {
        return GateMeta::resolve(
            $user, $this->getChannelId(), $this->getTransport(),
            $this->getMessageId(), $this->getDialogId());
    }
}