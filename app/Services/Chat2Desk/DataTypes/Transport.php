<?php

namespace App\Services\Chat2Desk\DataTypes;

use Illuminate\Contracts\Support\Arrayable;

class Transport implements Arrayable
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var AttachmentCapabilities
     */
    protected $fromClient;

    /**
     * @var AttachmentCapabilities
     */
    protected $toClient;

    /**
     * Transport constructor.
     *
     * @param $id
     * @param $fromClient
     * @param $toClient
     */
    public function __construct($id, $fromClient, $toClient)
    {
        $this->id = $id;
        $this->fromClient = $fromClient;
        $this->toClient = $toClient;
    }

    /**
     * @param $key
     * @param $data
     *
     * @return Transport
     */
    public static function make($key, $data)
    {
        $fromClient = new AttachmentCapabilities($data->from_client);
        $toClient = new AttachmentCapabilities($data->to_client);

        return new self($key, $fromClient, $toClient);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return AttachmentCapabilities
     */
    public function getFromClient()
    {
        return $this->fromClient;
    }

    /**
     * @return AttachmentCapabilities
     */
    public function getToClient()
    {
        return $this->toClient;
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'from_client' => $this->fromClient->toArray(),
            'to_client' => $this->toClient->toArray(),
        ];
    }
}