<?php

namespace App\Services\Chat2Desk\Requests\Keyboard;

use App\Common\TextProcessor;
use Illuminate\Contracts\Support\Arrayable;

abstract class BaseButton implements Arrayable
{
    const COLOR_WHITE = 'white';
    const COLOR_RED = 'red';
    const COLOR_BLUE = 'blue';
    const COLOR_GREEN = 'green';

    /**
     * @var string
     */
    public $text;

    /**
     * @var string
     */
    public $color;

    /**
     * BaseButton constructor.
     *
     * @param $text
     * @param string $color
     */
    public function __construct($text, $color = self::COLOR_WHITE)
    {
        $this->text = $text;
        $this->color = $color;
    }

    /**
     * @param array $data
     *
     * @return \App\Services\Chat2Desk\Requests\Keyboard\BaseButton[]
     */
    public static function collectionFromArray(array $data)
    {
        return collect($data)->map(function ($data) {
            return is_array($data) ? self::fromArray($data) : null;
        })->filter()->all();
    }

    /**
     * @param \App\Common\TextProcessor $processor
     *
     * @return $this
     */
    public function process(TextProcessor $processor)
    {
        $this->text = $processor->process($this->text);

        return $this;
    }

    /**
     * @param array $data
     *
     * @return \App\Services\Chat2Desk\Requests\Keyboard\BaseButton|null
     */
    public static function fromArray(array $data)
    {
        $type = $data['type'] ?? 'reply';

        switch ($type) {
            case 'reply': return ReplyButton::fromArray($data);
            case 'url': return UrlButton::fromArray($data);
            default: return null;
        }
    }

    /**
     * @return string
     */
    abstract public function getType();

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'type' => $this->getType(),
            'text' => $this->text,
            'color' => $this->color,
        ];
    }
}