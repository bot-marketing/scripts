<?php

namespace App\Services\Chat2Desk\Requests\Keyboard;

use Illuminate\Contracts\Support\Arrayable;

class Keyboard implements Arrayable
{
    /**
     * @var array
     */
    protected $buttons;

    /**
     * Keyboard constructor.
     *
     * @param array $buttons
     */
    public function __construct(array $buttons)
    {
        $this->buttons = $buttons;
    }

    /**
     * @param array $data
     *
     * @return \App\Services\Chat2Desk\Requests\Keyboard\Keyboard|null
     */
    public static function fromArray(array $data)
    {
        if (!isset($data['buttons']) || !is_array($data['buttons'])) return null;

        $buttons = BaseButton::collectionFromArray($data['buttons']);

        return $buttons ? new self($buttons) : null;
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'buttons' => collect($this->buttons)->toArray(),
        ];
    }
}