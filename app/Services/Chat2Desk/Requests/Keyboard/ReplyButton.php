<?php

namespace App\Services\Chat2Desk\Requests\Keyboard;

class ReplyButton extends BaseButton
{
    /**
     * @var string
     */
    public $payload;

    /**
     * ReplyButton constructor.
     *
     * @param $text
     * @param string $payload
     * @param string $color
     */
    public function __construct($text, $payload = null, $color = self::COLOR_WHITE)
    {
        parent::__construct($text, $color);

        $this->payload = $payload;
    }

    /**
     * @param array $data
     *
     * @return \App\Services\Chat2Desk\Requests\Keyboard\ReplyButton|null
     */
    public static function fromArray(array $data)
    {
        if (!isset($data['text'])) return null;

        return new self($data['text'], $data['payload'] ?? null, $data['color'] ?? null);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'reply';
    }

    /**
     * @return string
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            'payload' => $this->payload,
        ]);
    }
}