<?php

namespace App\Services\Chat2Desk\Requests\Keyboard;

class UrlButton extends BaseButton
{
    /**
     * @var string
     */
    public $url;

    /**
     * UrlButton constructor.
     *
     * @param string $text
     * @param string $url
     * @param string $color
     */
    public function __construct($text, $url, $color = self::COLOR_WHITE)
    {
        parent::__construct($text, $color);

        $this->url = $url;
    }

    /**
     * @param array $data
     *
     * @return \App\Services\Chat2Desk\Requests\Keyboard\UrlButton|null
     */
    public static function fromArray(array $data)
    {
        if (!isset($data['text']) || !isset($data['url'])) return null;

        return new self($data['text'], $data['url'], $data['color'] ?? null);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'url';
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [ 'url' => $this->url ]);
    }
}