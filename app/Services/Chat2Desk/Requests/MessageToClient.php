<?php

namespace App\Services\Chat2Desk\Requests;

class MessageToClient extends SendMessage
{
    /**
     * @var boolean
     */
    protected $openDialog;

    /**
     * @var \App\Services\Chat2Desk\Requests\Keyboard\Keyboard|null
     */
    protected $keyboard;

    /**
     * @var \App\Services\Chat2Desk\Requests\Keyboard\BaseButton[]|null
     */
    protected $inlineButtons;

    /**
     * @param $value
     *
     * @return $this
     */
    public function setKeyboard($value)
    {
        $this->keyboard = $value;

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setInlineButtons($value)
    {
        $this->inlineButtons = $value;

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setOpenDialog($value)
    {
        $this->openDialog = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'autoreply';
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            'open_dialog' => $this->openDialog,
            'keyboard' => $this->keyboard ? $this->keyboard->toArray() : null,
            'inline_buttons' => $this->inlineButtons ? collect($this->inlineButtons)->toArray() : null,
        ]);
    }
}