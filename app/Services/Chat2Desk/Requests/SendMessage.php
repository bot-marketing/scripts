<?php

namespace App\Services\Chat2Desk\Requests;

abstract class SendMessage extends BaseRequest
{
    /**
     * @var int
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $text;

    /**
     * @var string
     */
    protected $transport;

    /**
     * @var int
     */
    protected $channelId;

    /**
     * @var string
     */
    protected $attachment;

    /**
     * SendMessage constructor.
     *
     * @param $clientId
     * @param $text
     */
    public function __construct($clientId, $text)
    {
        $this->clientId = $clientId;
        $this->text = $text;
    }

    /**
     * @return string
     */
    abstract public function getType();

    /**
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setTransport($value)
    {
        $this->transport = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getChannelId()
    {
        return $this->channelId;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function setChannelId($value)
    {
        $this->channelId = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setAttachment($value)
    {
        $this->attachment = $value;

        return $this;
    }

    /**
     * @return array
     */
    protected function buildAttachment()
    {
        if ( ! $this->attachment) {
            return [];
        }

        if (strcasecmp(\File::extension($this->attachment), 'pdf') === 0) {
            return [ 'pdf' => $this->attachment ];
        }

        return [ 'attachment' => $this->attachment ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_filter(array_merge([
            'client_id' => $this->clientId,
            'text' => $this->text,
            'type' => $this->getType(),
            'channel_id' => $this->channelId,
            'transport' => $this->transport,
        ], $this->buildAttachment()));
    }

}