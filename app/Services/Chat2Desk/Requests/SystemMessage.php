<?php

namespace App\Services\Chat2Desk\Requests;

class SystemMessage extends SendMessage
{
    /**
     * @return string
     */
    public function getType()
    {
        return 'system';
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
           'open_dialog' => false,
        ]);
    }
}