<?php

namespace App\Services\Chat2Desk;

use Illuminate\Contracts\Support\MessageBag;

class ValidationException extends ClientErrorException
{
    /**
     * @var MessageBag
     */
    protected $errors;

    /**
     * ValidationException constructor.
     *
     * @param MessageBag $errors
     */
    public function __construct(MessageBag $errors)
    {
        parent::__construct('Validation failed.');

        $this->errors = $errors;
    }

    /**
     * @return MessageBag
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Report the exception.
     */
    public function report()
    {
        \Log::error($this->message, [
            'errors' => $this->errors->toArray(),
            'exception' => $this,
        ]);
    }
}