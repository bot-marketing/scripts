<?php

namespace App\Services\Crispb;

class TemplateProcessor extends \PhpOffice\PhpWord\TemplateProcessor
{
    /**
     * @param $search
     * @param $path
     *
     * @throws \Exception
     */
    public function replaceImage($search, $path)
    {
        $search = preg_quote('${'.$search.'}');

        if (preg_match('/<pic[^>]*descr="'.$search.'"[^>]*>/ui', $this->tempDocumentMainPart, $matches)) {
            if (preg_match('/name="(?<name>[^"]+)"/ui', $matches[0], $matches)) {
                $this->zipClass->pclzipAddFile($path, "word/media/{$matches['name']}");
            } else {
                throw new \Exception('Failed to resolve file name');
            }
        } else {
            throw new \Exception("Variable not found");
        }
    }
}