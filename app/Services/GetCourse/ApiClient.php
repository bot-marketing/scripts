<?php

namespace App\Services\GetCourse;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class ApiClient
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $token;

    /**
     * ApiClient constructor.
     *
     * @param $domain
     * @param $token
     */
    public function __construct($domain, $token)
    {
        $this->client = new Client([
            'base_uri' => "https://$domain/pl/api/",
            'verify' => false,
            'connect_timeout' => 10,
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $this->token = $token;
    }

    /**
     * @param $resource
     * @param $action
     * @param $params
     *
     * @return mixed
     */
    public function post($resource, $action, $params)
    {
        return $this->processResponse($this->client->post($resource, [
            'form_params' => [
                'key' => $this->token,
                'action' => $action,
                'params' => base64_encode(json_encode($params)),
            ],
        ]));
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface $response
     *
     * @return mixed
     *
     * @throws \App\Services\GetCourse\ApiException
     */
    protected function processResponse(ResponseInterface $response)
    {
        if ($code = $response->getStatusCode() !== 200) {
            throw new ApiException('Http Error '.$code, $code);
        }

        if (!$contents = $response->getBody()->getContents()) {
            throw new ApiException('Invalid response');
        }

        $data = \GuzzleHttp\json_decode($contents, true);

        if (!isset($data['success']) || !$data['success']) {
            throw new ApiException('Invalid response');
        }

        $response->getBody()->close();

        return $data['result'];
    }
}