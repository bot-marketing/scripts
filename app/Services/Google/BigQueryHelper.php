<?php

namespace App\Services\Google;

use Google\Client;
use Google\Service\Bigquery\TableDataInsertAllRequest;
use Google\Service\Bigquery\TableDataInsertAllResponse;

class BigQueryHelper
{
    protected \Google_Service_Bigquery $service;

    protected string $projectId;
    protected ?string $quotaUser;

    public function __construct(string $projectId, string $quotaUser = null, string $credentials = null)
    {
        $this->projectId = $projectId;
        $this->quotaUser = $quotaUser;
        $this->service = new \Google_Service_Bigquery(app(\Google_Client::class, [ $credentials ]));
        $this->service->getClient()->setScopes([ \Google_Service_Bigquery::BIGQUERY_INSERTDATA, \Google_Service_Bigquery::BIGQUERY ]);
    }

    public function insertAll(string $datasetId, string $tableId, $body): TableDataInsertAllResponse
    {
        return $this->service->tabledata->insertAll($this->projectId, $datasetId, $tableId, $body, [
            'quotaUser' => $this->quotaUser,
        ]);
    }
}