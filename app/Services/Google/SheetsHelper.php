<?php

namespace App\Services\Google;

use Carbon\Carbon;
use Google\Client;
use Google\Service\Sheets;
use Google\Service\Sheets\ValueRange;
use Illuminate\Support\Collection;

class SheetsHelper
{
    const TIME_FORMATS = [
        '^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(.\d{3,6})?(Z|[+-]\d{2}:\d{2})?$' => 'DATE_TIME',
        '^\d{4}-\d{2}-\d{2} \d{2}:\d{2}(:\d{2})?$' => 'DATE_TIME',
        '^\d{4}-\d{2}-\d{2}$' => 'DATE',
        '^\d{2}([.\/])\d{2}\1\d{4} \d{2}:\d{2}(:\d{2})?$' => 'DATE_TIME',
        '^\d{2}:\d{2}(:\d{2})?$' => 'TIME',
        '^\d{2}([.\/])\d{2}\1\d{4}$' => 'DATE',
    ];

    /**
     * @var
     */
    protected $fileId;

    /**
     * @var \Google_Service_Sheets
     */
    protected $service;

    /**
     * @var null
     */
    protected $quotaUser;

    protected bool $parseDates;

    protected ?string $tz;

    /**
     * SheetsHelper constructor.
     *
     * @param $fileId
     */
    public function __construct($fileId, $serviceAuthFile = null, $quotaUser = null, bool $parseDates = false, ?string $tz = null)
    {
        $this->fileId = $fileId;
        $this->service = $this->createServiceFromAuthFile($serviceAuthFile);
        $this->quotaUser = $quotaUser;
        $this->parseDates = $parseDates;
        $this->tz = $tz;
    }

    /**
     * @param array $data
     * @param int $sheetId
     *
     * @return bool
     */
    public function addRow(array $data, $sheetId = 0)
    {
        $values = collect($data)->map(function ($value) {
            $cellData = new \Google_Service_Sheets_CellData();
            $cellValue = new \Google_Service_Sheets_ExtendedValue();

            if (is_array($value)) {
                $cellValue->setStringValue(json_encode($value, JSON_UNESCAPED_UNICODE));
            } elseif (is_null($value)) {
                $cellValue->setStringValue('');
            } elseif (is_bool($value)) {
                $cellValue->setBoolValue($value);
            } elseif ((string)$value === '0' || preg_match('/^[1-9]\d*(\.\d+)?$/', $value)) {
                $cellValue->setNumberValue($value);
            } elseif ($date = $this->parseDate($value)) {
                $cellValue->setNumberValue($date[0]);
                $cellData->setUserEnteredFormat($date[1]);
            } else {
                $cellValue->setStringValue((string)$value);
            }

            $cellData->setUserEnteredValue($cellValue);

            return $cellData;
        });

        $row = new \Google_Service_Sheets_RowData();
        $row->setValues($values->all());

        $appendRequest = new \Google_Service_Sheets_AppendCellsRequest();
        $appendRequest->setSheetId($sheetId);
        $appendRequest->setRows($row);
        $appendRequest->setFields('userEnteredValue,userEnteredFormat');

        $request = new \Google_Service_Sheets_Request();
        $request->setAppendCells($appendRequest);

        $batchRequest = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
            'requests' => [ $request ],
        ]);

        $response = $this->service->spreadsheets->batchUpdate($this->fileId, $batchRequest, [ 'quotaUser' => $this->quotaUser ]);

        return $response->valid();
    }

    /**
     * @param $sheetId
     * @param $range
     * @param bool $asObject
     *
     * @return \Illuminate\Support\Collection|mixed
     */
    public function getRows($range, $asObject = false)
    {
        return collect($this->service->spreadsheets_values->get($this->fileId, $range))
            ->filter()
            ->when($asObject, function (Collection $items) {
                $headers = $items->shift();
                $headerSize = count($headers);

                return $items->map(function ($item) use ($headers, $headerSize) {
                    if (count($item) > $headerSize) {
                        $item = array_slice($item, 0, $headerSize);
                    } else {
                        $item = array_pad($item, count($headers), null);
                    }

                    return array_combine($headers, $item);
                });
            });
    }

    public function updateRow($range, $data): array
    {
        $values = new ValueRange();
        $values->setValues([ array_map(function ($v) { return is_null($v) ? '' : $v; }, $data) ]);

        return (array)$this->service->spreadsheets_values->update($this->fileId, $range, $values, [
            'valueInputOption' => 'USER_ENTERED',
            'quotaUser' => $this->quotaUser,
        ])->toSimpleObject();
    }

    public function batchUpdate(array $requests): array
    {
        $batchRequest = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest(compact('requests'));

        $response = $this->service->spreadsheets->batchUpdate($this->fileId, $batchRequest, [ 'quotaUser' => $this->quotaUser ]);

        return (array)$response->toSimpleObject();
    }

    protected function createServiceFromAuthFile($serviceAuthFile): Sheets
    {
        $client = app(\Google_Client::class, [ $serviceAuthFile ]);

        $client->addScope(Sheets::SPREADSHEETS);

        return new Sheets($client);
    }

    function dateFormat($type): Sheets\CellFormat
    {
        return tap(new Sheets\CellFormat(), function (Sheets\CellFormat $format) use ($type) {
            $format->setNumberFormat(
                tap(new Sheets\NumberFormat(), function (Sheets\NumberFormat $format) use ($type) {
                    $format->setType($type);
                })
            );
        });
    }

    protected function parseDate($value)
    {
        if (!$this->parseDates || is_numeric($value)) return false;

        foreach (self::TIME_FORMATS as $format => $sheetFormat) {
            if (1 !== preg_match('/'.$format.'/', $value)) continue;

            try {
                $date = Carbon::parse($value, $this->tz)->setTimezone($this->tz);
            }

            catch (\Throwable $e) {
                return false;
            }

            $format = $this->dateFormat($sheetFormat);

            if ($sheetFormat === 'TIME') return [ $date->secondsSinceMidnight() / 86400.0, $format ];

            return [ (float)$date->diffInDays('1899-12-30') + $date->secondsSinceMidnight() / 86400.0, $format ];
        }

        return false;
    }
}