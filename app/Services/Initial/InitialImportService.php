<?php

namespace App\Services\Initial;


use App\Modules\Initials\Initial;

class InitialImportService
{
    /**
     * @var
     */
    private $file;
    private $language;
    private $type;

    /**
     * InitialImportService constructor.
     *
     * @param $file
     * @param $language
     * @param $type
     */
    public function __construct($file, $language, $type)
    {
        $this->file = $file;
        $this->language = $language;
        $this->type = $type;
    }

    /**
     * Import db
     */
    public function Import()
    {
        Initial::where('type', '=', $this->type)
            ->where('language', '=', $this->language)
            ->delete();

        $initials = [];
        $handle = fopen($this->file, 'r');

        if ($handle !== false) {
            fgetcsv($handle, 1000, ";");
            while (($data = fgetcsv($handle, 1000, ";")) !== false) {
                $initials[] = [
                    'type'         => $this->type,
                    'language'     => $this->language,
                    'value'        => $data[1],
                    'sex'          => $data[2] == 'Ж'
                        ? Initial::WOMAN
                        : ($data[2] == 'М' ? Initial::MAN : 0),
                    'people_count' => (int)$data[3],
                ];

                if (count($initials) > 1000) {
                    Initial::insert($initials);
                    $initials = [];
                }
            }
            fclose($handle);
        }

        Initial::insert($initials);
    }

}