<?php

namespace App\Services\Motorland;


use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Psr\Http\Message\ResponseInterface;


class ApiClient
{
    /**
     * @var string
     */
    const DOMAIN_PROD = 'https://db.motorland.by';
    const DOMAIN_TEST = 'https://test.db.motorland.by';

    const METHOD_INFO = '/Chat2Desk/GetClient';
    const METHOD_SAVE = '/Chat2Desk/SaveClient';
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * @var
     */
    private $production;

    /**
     * ApiClient constructor.
     *
     * @param $production
     */
    public function __construct($production = false, $api_url = null)
    {
        $this->production = $production;
        $this->api_url = $api_url;

        $this->client = new Client([
            'base_uri'    => $this->getUrl(),
            'verify'      => false,
            'http_errors' => false,
        ]);
    }

    /**
     * @param array $data
     *
     * @return Collection
     */
    public function info($data): Collection
    {
        return $this->post(self::METHOD_INFO, $data);
    }

    /**
     * @param $data
     *
     * @return Collection
     */
    public function save($data): Collection
    {
        return $this->post(self::METHOD_SAVE, $data);
    }

    /**
     * @param string $method
     * @param array  $data
     *
     * @return Collection
     */
    public function post($method, $data): Collection
    {
        return $this->parseResponse($this->client->post($this->getUrl($method), [
            'form_params' => $data,
        ]));
    }


    /**
     * @param ResponseInterface $response
     *
     * @return Collection
     * @throws ApiException
     */
    protected function parseResponse(ResponseInterface $response): Collection
    {
        $data = \GuzzleHttp\json_decode($response->getBody()->getContents() ?: '{}', true);

        $response->getBody()->close();

        $response = Collection::make($data);

        if ($response->get('error')) {
            throw new ApiException($response->get('error'));
        }

        return $response;
    }

    /**
     * @param null|string $method
     *
     * @return string
     */
    public function getUrl($method = null): string
    {
        if ($this->api_url) {
            return $this->api_url;
        }

        $domain = $this->production
            ? self::DOMAIN_PROD
            : self::DOMAIN_TEST;

        return $method
            ? $domain . $method
            : $domain;
    }

}