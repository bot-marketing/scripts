<?php

namespace App\Services\Webinar;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class ApiClient
{
    /**
     * ApiClient constructor.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->client = new Client([
            'base_uri' => 'https://userapi.webinar.ru/v3/',
            'connect_timeout' => 10,
            'headers' => [
                'X-Auth-Token' => $token,
            ],
            'verify' => false,
            'http_errors' => false,
        ]);
    }

    /**
     * @param $method
     * @param array $query
     *
     * @return mixed
     */
    public function get($method, $query = [])
    {
        return $this->parseResponse($this->client->get($method, compact('query')));
    }

    /**
     * @param $method
     * @param $params
     *
     * @return mixed
     */
    public function post($method, $params)
    {
        return $this->parseResponse($this->client->post($method, [ 'form_params' => $params ]));
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface $response
     *
     * @return mixed
     * @throws \App\Services\Webinar\ApiException
     */
    protected function parseResponse(ResponseInterface $response)
    {
        $data = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

        $response->getBody()->close();

        if (isset($data['error'])) {
            throw new ApiException($data['error']['message'], $data['error']['code']);
        }

        return $data;
    }
}