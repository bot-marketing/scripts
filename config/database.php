<?php

return [
  'default' => env('DB_CONNECTION', 'mysql'),
  'migrations' => 'migrations',
  'connections' => [
    'mysql' => [
      'driver' => 'mysql',
      'host' => env('DB_HOST'),
      'database' => env('DB_DATABASE'),
      'port' => env('DB_PORT'),
      'username' => env('DB_USERNAME'),
      'password' => env('DB_PASSWORD'),
      'charset' => 'utf8',
      'collation' => 'utf8_unicode_ci',
      'prefix' => '',
      'strict' => false,
    ],
    'pgsql' => [
      'driver' => 'pgsql',
      'host' => env('DB_PG_HOST', 'localhost'),
      'port' => env('DB_PG_PORT', 5432),
      'database' => env('DB_PG_DATABASE', 'forge'),
      'username' => env('DB_PG_USERNAME', 'forge'),
      'password' => env('DB_PG_PASSWORD', ''),
      'charset' => 'utf8',
      'prefix' => env('DB_PREFIX', ''),
      'schema' => 'public',
      'options' => array(
        "sslmode" => "verify-full",
      )
    ],
  ],
  'redis' => [
    'cluster' => false,
    'default' => [
      'host' => env('REDIS_HOST', 'localhost'),
      'password' => env('REDIS_PASSWORD', null),
      'port' => env('REDIS_PORT', 6379),
      'database' => 0,
    ],
  ],
];
