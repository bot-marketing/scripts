<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageQueues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_queues', function (Blueprint $table) {
            $table->increments('id');
            $table->char('hash', 32)->charset('ascii')->unique();
            $table->unsignedTinyInteger('provider');
            $table->string('provider_auth');
            $table->string('name');
            $table->text('text')->nullable();
            $table->string('file_url', 500)->nullable();
            $table->timestamp('time')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->text('error')->nullable();

            $table->index([ 'status', 'time' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_queues');
    }
}
