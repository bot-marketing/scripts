<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageQueuesContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_queues_contacts', function (Blueprint $table) {
            $table->unsignedInteger('queue_id');
            $table->decimal('phone', 11, 0);

            $table->unique([ 'queue_id', 'phone' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_queues_contacts');
    }
}
