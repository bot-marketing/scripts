<?php

use App\Modules\Initials\Initial;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('initials', function (Blueprint $table) {
            $table->increments('id');

            $table->tinyInteger('type');
            $table->string('language')->default('ru');
            $table->integer('sex');
            $table->integer('people_count');
            $table->string('value');
        });

        DB::statement('ALTER TABLE initials ADD FULLTEXT INDEX IDX_ngram_value(`value`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('initials');
    }
}
