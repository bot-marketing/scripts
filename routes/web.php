<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * @var \Laravel\Lumen\Routing\Router $router
 */

$router->group([ 'prefix' => 'SvfuhHZuWw' ], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('start', 'KonturController@start');
    $router->post('bonus', 'KonturController@bonus');
});

$router->group([ 'prefix' => 'mWVHGNYBiOqERzyD' ], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('checkPhone', 'ZeppelinController@checkPhone');
    $router->post('noPhoneMessage', 'ZeppelinController@noPhoneMessage');
    $router->post('addRequest', 'ZeppelinController@addRequest');
    $router->post('clsWebhook', 'ZeppelinController@clsWebhook');
});

$router->group([ 'prefix' => 'DTsAcKDNYP' ], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('address', 'Partner31Controller@address');
});

$router->group([ 'prefix' => 'lyk5XowOlqWk' ], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('clientInfo', 'MotorlandController@clientInfo');
    $router->post('clientSave', 'MotorlandController@clientSave');
});

$router->group([ 'prefix' => 'uaWqWTjf7Qrq' ], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('promocode', 'AdcomeController@promocode');
});

$router->group([ 'prefix' => '9D5Bh6w2IA' ], function ($router) {
   $router->post('makeBook', 'BookersonController@makeBook');
   $router->post('discount', 'BookersonController@discount');
});

$router->group([ 'prefix' => 'bitrix' ], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('deal', 'BitrixController@deal');
    $router->post('lead/update', 'BitrixController@leadUpdate');
    $router->post('lead', 'BitrixController@lead');
    $router->post('addCommentToLead', 'BitrixController@addCommentToLead');
});

$router->group([ 'prefix' => 'amo' ], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('addLead', 'AmoController@addLead');
    $router->post('updateLead', 'AmoController@updateLead');
    $router->post('addNoteToLead', 'AmoController@addNoteToLead');
    $router->post('leadsCallback', 'AmoController@leadsCallback');
    $router->post('findLead', 'AmoController@findLead');
    $router->post('updateContact', 'AmoController@updateContact');
});

$router->group([ 'prefix' => 'getcourse' ], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('addUser', 'GetCourseController@addUser');
});

$router->group([ 'prefix' => 'common' ], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('startTunnelBySwitch', 'CommonController@startTunnelBySwitch');
    $router->post('sleep', 'CommonController@sleep');
    $router->post('parseName', 'CommonController@getInitial');
    $router->post('storeValue', 'CommonController@storeValue');
    $router->post('restoreValue', 'CommonController@restoreValue');
});

$router->group([ 'prefix' => 'google' ], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('sheets/addRow', 'GoogleSheetsController@addRow');
    $router->post('sheets/getRows', 'GoogleSheetsController@getRows');
    $router->post('sheets/updateRow', 'GoogleSheetsController@updateRow');
    $router->post('sheets/batchUpdate', 'GoogleSheetsController@batchUpdate');
    $router->post('bigquery/insertAll', 'GoogleBigQueryController@insertAll');
});

$router->group([ 'prefix' => 'webinar' ], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('events/{event}/latestSession', 'WebinarController@latestEventSession');
    $router->post('eventSessions/{event}/register', 'WebinarController@registerToEventSession');
});

$router->group(['prefix'=>'bizon365'], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('page','Bizon365Controller@getAllPages');
    $router->post('register','Bizon365Controller@registerUserOnWebinar');
    $router->post('subscribers','Bizon365Controller@subscriber');
});

$router->group(['prefix' =>'bibikol'], function (\Laravel\Lumen\Routing\Router $router){
    $router->post('image','BibicolController@image');
    $router->post('check','BibicolController@addCheck');
    $router->post('receipt','BibicolController@retrieveResultOneReceipt');
});

$router->group(['prefix' =>'retailcrm'], function (\Laravel\Lumen\Routing\Router $router){
    $router->post('orders/create','RetailCrmController@createOrder');
    $router->post('orders/edit','RetailCrmController@editOrder');
});

$router->post('facedetector/upload-and-search', 'FacedetectorController');

$router->post('message-queues', 'MessageQueuesController@push');

$router->group(['prefix' =>'8jQXRDk7m8ga'], function (\Laravel\Lumen\Routing\Router $router) {
    $router->post('pg_query', 'OpArtController');
});

//$router->post('cirspb', 'CirspbController');
//$router->post('cirspb/callback', 'CirspbController@callback');

if (app()->environment('local')) {
    $router->get('test', 'ExampleController@test');
}
